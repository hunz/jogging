var pg = require('pg');
var path = require('path');
var moment = require('moment');

var env = require(path.join(__dirname, '..', 'server', 'config', 'environment'));

var client;

var i; // query index
var j; // current query execution count

// http://uinames.com/
var usernames = [
  'john.doe',
  'frances.valdez',
  'bohdan.kostrec',
  'relu.firulescu',
  'samuel.garrett',
  'juan.hansen',
  'kenneth.brooks',
  'carol.hill',
  'kenneth.carpenter',
  'thiago.negri'
];

var queries = [

  {
    query: 'INSERT INTO users (username, password, role) VALUES ($1, md5($2), $3)',
    count: 2 + usernames.length,
    params: function(i) {
      switch(i) {
        case 0:
          return ['admin', 'secret', 'A'];
        case 1:
          return ['user_manager', 'secret', 'U'];
        default:
          return [
            usernames[i - 2],
            'secret',
            'C'
          ];
      }
    }
  },

  {
    query: 'INSERT INTO times (user_id, moment, distance, time) VALUES ($1, $2, $3, $4)',
    count: 300 * usernames.length,
    timeMoment: moment().startOf('day'),
    params: function(i) {
      var userId = (i % usernames.length) + 3;
      if (userId == 3) {
        this.timeMoment = this.timeMoment.subtract(1, 'days');
      }
      if (Math.random() < 0.2) {
        // Randomly generate multiple entries for the same user and same day
        userId = Math.floor((Math.random() * usernames.length) + 3);
      }
      if (Math.random() < 0.3) {
        // Do not generate jogging time for every day for every one
        return false;
      }
      var aproxSpeed = (Math.random() * 5) + 4; // 4 ~ 9km/h
      var time = (Math.random() * 2) + 0.25; // 15min ~ 2h15min
      var distance = aproxSpeed * time;
      return [ userId, this.timeMoment, Math.floor(distance * 1000), Math.floor(time * 60) ];
    }
  }

];

var onEnd;

var callback = function() {
  var query = null;
  var params = null;

  j += 1;
  if (j >= queries[i].count) {
    j = 0;
    i += 1;
  }

  if (i >= queries.length) {
    client.end();
    if (onEnd) {
      onEnd();
    }
    return;
  }

  query = queries[i].query;
  params = queries[i].params(j);

  if (!params) {
    return callback();
  }

  if (env.log.databaseScripts) {
    console.log('>', query + ' | ' + params);
  }

  client.query(query, params).on('end', callback);
};

var main = function(endCallback) {
  onEnd = endCallback;

  i = 0;
  j = -1;

  client = new pg.Client(env.databaseUrl);
  client.connect();
  callback();
};

if (!module.parent) {
  main();
}

module.exports = main;
