var pg = require('pg');
var path = require('path');

var env = require(path.join(__dirname, '..', 'server', 'config', 'environment'));

var client;

var i; // query index
var j; // params index

var queries = [

  ['INSERT INTO users (username, password, role) VALUES ($1, md5($2), $3)',

    ['admin', 'secret', 'A'],
    ['user_manager', 'secret', 'U'],
    ['common_user', 'secret', 'C'],

  ],

  ['INSERT INTO times (user_id, moment, distance, time) VALUES ($1, $2, $3, $4)',
  
    // Admin times
    [1, '2016-01-01', 1000,  60],
    [1, '2016-01-02', 2000, 120],
    [1, '2016-01-03', 3000, 180],
    [1, '2016-01-04', 4000, 240],

    // User manager times
    [2, '2016-01-01', 1000,  60],
    [2, '2016-01-02', 2000, 120],
    [2, '2016-01-03', 3000, 180],
    [2, '2016-01-04', 4000, 240],

    // Common user times
    [3, '2016-01-01', 1000,  60],
    [3, '2016-01-02', 2000, 120],
    [3, '2016-01-03', 3000, 180],
    [3, '2016-01-04', 4000, 240],

  ],

];

var onEnd;

var callback = function() {
  var query = null;
  var params = null;

  j += 1;
  if (j >= queries[i].length) {
    j = 1;
    i += 1;
  }

  if (i >= queries.length) {
    client.end();
    if (onEnd) { 
      onEnd(); 
    }
    return;
  }

  query = queries[i][0];
  params = queries[i][j];

  if (env.log.databaseScripts) {
    console.log('>', query + ' | ' + params);
  }

  client.query(query, params).on('end', callback);
};

var main = function(endCallback) {
  onEnd = endCallback;

  i = 0;
  j = 0;

  client = new pg.Client(env.databaseUrl);
  client.connect();
  callback();
};

if (!module.parent) {
  main();
}

module.exports = main;
