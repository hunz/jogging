/*
 * DEV:
 * create role jogging_user login password 'secret';
 * create database jogging;
 * grant all on database jogging to jogging_user;
 *
 * TEST:
 * create role jogging_user_test login password 'secret_test';
 * create database jogging_test;
 * grant all on database jogging_test to jogging_user_test;
 */

var pg = require('pg');
var path = require('path');

var env = require(path.join(__dirname, '..', 'server', 'config', 'environment'));

var client;
var i = -1;

var queries = [

  // Drop old objects
  'DROP TABLE IF EXISTS times',
  'DROP TABLE IF EXISTS sessions',
  'DROP TABLE IF EXISTS users',

  // Create new objects
  'CREATE TABLE users (id SERIAL PRIMARY KEY, username VARCHAR(255) UNIQUE, password CHAR(32), role CHAR(1))',
  'CREATE TABLE sessions (id SERIAL PRIMARY KEY, user_id INTEGER REFERENCES users (id), token CHAR(32), timeout TIMESTAMP WITH TIME ZONE)',
  'CREATE TABLE times (id SERIAL PRIMARY KEY, user_id INTEGER REFERENCES users (id), moment DATE, distance INTEGER, time INTEGER)',

];

var onEnd;

var callback = function() {
  i += 1;
  if (i >= queries.length) {
    client.end();
    if (onEnd) { onEnd(); }
  } else {
    if (env.log.databaseScripts) {
      console.log('>', queries[i]);
    }
    client.query(queries[i]).on('end', callback);
  }
};

var main = function(endCallback) {
  onEnd = endCallback;
  i = -1;
  client = new pg.Client(env.databaseUrl);
  client.connect();
  callback();
};

if (!module.parent) {
  main();
}

module.exports = main;

