var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var bodyParser = require('body-parser');

var session = require(path.join(__dirname, 'server', 'routes', 'api', 'session-api'));
var users = require(path.join(__dirname, 'server', 'routes', 'api', 'users-api'));
var times = require(path.join(__dirname, 'server', 'routes', 'api', 'times-api'));
var reports = require(path.join(__dirname, 'server', 'routes', 'api', 'report-api'));
var docs = require(path.join(__dirname, 'server', 'routes', 'api-docs'));

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'client', 'views'));
app.set('view engine', 'ejs');

//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
if (app.get('env') === 'development') {
  app.use(logger('dev'));
}
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use(express.static(path.join(__dirname, 'client', 'public')));
app.get('/', function(req, res) {
  res.sendFile('index.html', { root: path.join(__dirname, 'client', 'public') });
});

app.use('/api-docs', docs);
app.use('/api/session', session);
app.use('/api/users', users);
app.use('/api/times', times);
app.use('/api/reports', reports);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});

module.exports = app;
