#!/bin/bash

# Make sure the database is in the seed state
NODE_ENV=test npm run db-create
NODE_ENV=test npm run db-seed

# Run the tests
npm run e2e-test

