var Promise = require('promise');
var request = require('supertest-as-promised');
var expect = require('expect.js');
var path = require('path');

var app = require(path.join(__dirname, '..', 'jogging-server'));

var utils = require(path.join(__dirname, 'utils'));
utils.config.app = app;

describe('/api/reports', function() {

  beforeEach(utils.dbCreateAndSeed);

  describe('/average-speed-and-distance-per-week', function() {

    it('should not be accessible by unauthorized request', function() {
      return utils.shouldRequireToken('get', '/api/reports/average-speed-and-distance-per-week');
    });

    it('should be accessible by admin', function() {
      return utils.authenticate('admin', 'secret').then(function(token) {
        return utils.shouldGet(token, '/api/reports/average-speed-and-distance-per-week', [
          { week: '2015-53', averageSpeed: 1, distance: 6000 },
          { week: '2016-01', averageSpeed: 1, distance: 4000 }
        ]);
      });
    });

    it('should be accessible by user manager', function() {
      return utils.authenticate('user_manager', 'secret').then(function(token) {
        return utils.shouldGet(token, '/api/reports/average-speed-and-distance-per-week', [
          { week: '2015-53', averageSpeed: 1, distance: 6000 },
          { week: '2016-01', averageSpeed: 1, distance: 4000 }
        ]);
      });
    });

    it('should be accessible by common user', function() {
      return utils.authenticate('common_user', 'secret').then(function(token) {
        return utils.shouldGet(token, '/api/reports/average-speed-and-distance-per-week', [
          { week: '2015-53', averageSpeed: 1, distance: 6000 },
          { week: '2016-01', averageSpeed: 1, distance: 4000 }
        ]);
      });
    });

  });

});
