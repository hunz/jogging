var Promise = require('promise');
var request = require('supertest-as-promised');
var expect = require('expect.js');
var path = require('path');

var app = require(path.join(__dirname, '..', 'jogging-server'));

var utils = require(path.join(__dirname, 'utils'));
utils.config.app = app;

describe('/api/times', function() {

  beforeEach(utils.dbCreateAndSeed);

  describe('unauthorized', function() {

    it('should not be able to list times', function() {
      return utils.shouldRequireToken('get', '/api/times');
    });

    it('should not be able to create time', function() {
      return utils.shouldRequireToken('post', '/api/times', { moment: '2016-01-01', distance: 1000, time: 1 });
    });

    it('should not be able to read time', function() {
      return utils.shouldRequireToken('get', '/api/times/1');
    });

    it('should not be able to update time', function() {
      return utils.shouldRequireToken('put', '/api/times/1', { distance: 1000 });
    });

    it('should not be able to delete time', function() {
      return utils.shouldRequireToken('delete', '/api/times/1');
    });

  });

  describe('admin', function() {

    var token;

    beforeEach(function() {
      return utils.authenticate('admin', 'secret').then(function(t) { token = t; });
    });

    it('should see times of all users when listing', function() {
      return utils.shouldGet(token, '/api/times', {
        times: [
          { id:  4, moment: '2016-01-04', userId: 1, user: { id: 1, username: 'admin'        }, distance: 4000, time: 240, averageSpeed: 1 },
          { id: 12, moment: '2016-01-04', userId: 3, user: { id: 3, username: 'common_user'  }, distance: 4000, time: 240, averageSpeed: 1 },
          { id:  8, moment: '2016-01-04', userId: 2, user: { id: 2, username: 'user_manager' }, distance: 4000, time: 240, averageSpeed: 1 },
          { id:  3, moment: '2016-01-03', userId: 1, user: { id: 1, username: 'admin'        }, distance: 3000, time: 180, averageSpeed: 1 },
          { id: 11, moment: '2016-01-03', userId: 3, user: { id: 3, username: 'common_user'  }, distance: 3000, time: 180, averageSpeed: 1 },
          { id:  7, moment: '2016-01-03', userId: 2, user: { id: 2, username: 'user_manager' }, distance: 3000, time: 180, averageSpeed: 1 },
          { id:  2, moment: '2016-01-02', userId: 1, user: { id: 1, username: 'admin'        }, distance: 2000, time: 120, averageSpeed: 1 },
          { id: 10, moment: '2016-01-02', userId: 3, user: { id: 3, username: 'common_user'  }, distance: 2000, time: 120, averageSpeed: 1 },
          { id:  6, moment: '2016-01-02', userId: 2, user: { id: 2, username: 'user_manager' }, distance: 2000, time: 120, averageSpeed: 1 },
          { id:  1, moment: '2016-01-01', userId: 1, user: { id: 1, username: 'admin'        }, distance: 1000, time: 60, averageSpeed: 1 },
          { id:  9, moment: '2016-01-01', userId: 3, user: { id: 3, username: 'common_user'  }, distance: 1000, time: 60, averageSpeed: 1 },
          { id:  5, moment: '2016-01-01', userId: 2, user: { id: 2, username: 'user_manager' }, distance: 1000, time: 60, averageSpeed: 1 }
        ]
      });
    });

    it('should be able to list times of self', function() {
      return utils.shouldGet(token, '/api/times?userId=1', {
        times: [
          { id: 4, moment: '2016-01-04', userId: 1, user: { id: 1, username: 'admin' }, distance: 4000, time: 240, averageSpeed: 1 },
          { id: 3, moment: '2016-01-03', userId: 1, user: { id: 1, username: 'admin' }, distance: 3000, time: 180, averageSpeed: 1 },
          { id: 2, moment: '2016-01-02', userId: 1, user: { id: 1, username: 'admin' }, distance: 2000, time: 120, averageSpeed: 1 },
          { id: 1, moment: '2016-01-01', userId: 1, user: { id: 1, username: 'admin' }, distance: 1000, time: 60, averageSpeed: 1 }
        ]
      });
    });

    it('should be able to list times of other user', function() {
      return utils.shouldGet(token, '/api/times?userId=2', {
        times: [
          { id: 8, moment: '2016-01-04', userId: 2, user: { id: 2, username: 'user_manager' }, distance: 4000, time: 240, averageSpeed: 1 },
          { id: 7, moment: '2016-01-03', userId: 2, user: { id: 2, username: 'user_manager' }, distance: 3000, time: 180, averageSpeed: 1 },
          { id: 6, moment: '2016-01-02', userId: 2, user: { id: 2, username: 'user_manager' }, distance: 2000, time: 120, averageSpeed: 1 },
          { id: 5, moment: '2016-01-01', userId: 2, user: { id: 2, username: 'user_manager' }, distance: 1000, time: 60, averageSpeed: 1 }
        ]
      });
    });

    it('should be able to create time for self', function() {
      return utils.shouldPostCreate(token, '/api/times',
        { moment: '2016-02-01', distance: 10000, time: 300 },
        { time: { id: 13, moment: '2016-02-01', userId: 1, user: { id: 1, username: 'admin' }, distance: 10000, time: 300, averageSpeed: 2 } }
      );
    });

    it('should be able to create time for other user', function() {
      return utils.shouldPostCreate(token, '/api/times',
        { userId: 2, moment: '2016-02-01', distance: 10000, time: 300 },
        { time: { id: 13, moment: '2016-02-01', userId: 2, user: { id: 2, username: 'user_manager' }, distance: 10000, time: 300, averageSpeed: 2 } }
      );
    });

    it('should be able to read time from self', function() {
      return utils.shouldGet(token, '/api/times/1', {
        time: { id: 1, moment: '2016-01-01', userId: 1, user: { id: 1, username: 'admin' }, distance: 1000, time: 60, averageSpeed: 1 }
      });
    });
    
    it('should be able to read time from other user', function() {
      return utils.shouldGet(token, '/api/times/5', {
        time: { id: 5, moment: '2016-01-01', userId: 2, user: { id: 2, username: 'user_manager' }, distance: 1000, time: 60, averageSpeed: 1 }
      });
    });

    it('should be able to edit time from self', function() {
      return utils.shouldPut(token, '/api/times/1', 
        { distance: 2000, time: 240 },
        { time: { id: 1, moment: '2016-01-01', userId: 1, user: { id: 1, username: 'admin' }, distance: 2000, time: 240, averageSpeed: 0.5 } }
      );
    });
    
    it('should be able to edit time from other user', function() {
      return utils.shouldPut(token, '/api/times/5', 
        { distance: 2000, time: 240 },
        { time: { id: 5, moment: '2016-01-01', userId: 2, user: { id: 2, username: 'user_manager' }, distance: 2000, time: 240, averageSpeed: 0.5 } }
      );
    });

    it('should be able to delete time from self', function() {
      return utils.shouldDeleteAndThenNotFound(token, '/api/times/1');
    });
    
    it('should be able to delete time from other user', function() {
      return utils.shouldDeleteAndThenNotFound(token, '/api/times/5');
    });

  });

  describe('user manager', function() {

    var token;

    beforeEach(function() {
      return utils.authenticate('user_manager', 'secret').then(function(t) { token = t; });
    });

    it('should only see self times when listing', function() {
      return utils.shouldGet(token, '/api/times', {
        times: [
          { id: 8, moment: '2016-01-04', userId: 2, user: { id: 2, username: 'user_manager' }, distance: 4000, time: 240, averageSpeed: 1 },
          { id: 7, moment: '2016-01-03', userId: 2, user: { id: 2, username: 'user_manager' }, distance: 3000, time: 180, averageSpeed: 1 },
          { id: 6, moment: '2016-01-02', userId: 2, user: { id: 2, username: 'user_manager' }, distance: 2000, time: 120, averageSpeed: 1 },
          { id: 5, moment: '2016-01-01', userId: 2, user: { id: 2, username: 'user_manager' }, distance: 1000, time: 60, averageSpeed: 1 }
        ]
      });
    });

    it('should be able to list self times', function() {
      return utils.shouldGet(token, '/api/times?userId=2', {
        times: [
          { id: 8, moment: '2016-01-04', userId: 2, user: { id: 2, username: 'user_manager' }, distance: 4000, time: 240, averageSpeed: 1 },
          { id: 7, moment: '2016-01-03', userId: 2, user: { id: 2, username: 'user_manager' }, distance: 3000, time: 180, averageSpeed: 1 },
          { id: 6, moment: '2016-01-02', userId: 2, user: { id: 2, username: 'user_manager' }, distance: 2000, time: 120, averageSpeed: 1 },
          { id: 5, moment: '2016-01-01', userId: 2, user: { id: 2, username: 'user_manager' }, distance: 1000, time: 60, averageSpeed: 1 }
        ]
      });
    });

    it('should not be able to list times of admin', function() {
      return utils.shouldForbidGet(token, '/api/times?userId=1');
    });

    it('should not be able to list times of other user', function() {
      return utils.shouldForbidGet(token, '/api/times?userId=3');
    });

    it('should be able to create time for self', function() {
      return utils.shouldPostCreate(token, '/api/times',
        { moment: '2016-02-01', distance: 10000, time: 300 },
        { time: { id: 13, moment: '2016-02-01', userId: 2, user: { id: 2, username: 'user_manager' }, distance: 10000, time: 300, averageSpeed: 2 } }
      );
    });

    it('should not be able to create time for other user', function() {
      return utils.shouldForbidPost(token, '/api/times', { userId: 1, moment: '2016-02-01', distance: 10000, time: 300 });
    });

    it('should be able to read time from self', function() {
      return utils.shouldGet(token, '/api/times/5', {
        time: { id: 5, moment: '2016-01-01', userId: 2, user: { id: 2, username: 'user_manager' }, distance: 1000, time: 60, averageSpeed: 1 }
      });
    });

    it('should not be able to read time from other user', function() {
      return utils.shouldForbidGet(token, '/api/times/1');
    });

    it('should be able to edit time from self', function() {
      return utils.shouldPut(token, '/api/times/5', 
        { distance: 2000, time: 240 },
        { time: { id: 5, moment: '2016-01-01', userId: 2, user: { id: 2, username: 'user_manager' }, distance: 2000, time: 240, averageSpeed: 0.5 } }
      );
    });
    
    it('should not be able to edit time from other user', function() {
      return utils.shouldForbidPut(token, '/api/times/1', { distance: 2000, time: 240 });
    });

    it('should be able to delete time from self', function() {
      return utils.shouldDeleteAndThenNotFound(token, '/api/times/5');
    });
    
    it('should not be able to delete time from other user', function() {
      return utils.shouldForbidDelete(token, '/api/times/1');
    });
    
  });

  describe('common user', function() {

    var token;

    beforeEach(function() {
      return utils.authenticate('common_user', 'secret').then(function(t) { token = t; });
    });

    it('should only see self times when listing', function() {
      return utils.shouldGet(token, '/api/times', {
        times: [
          { id: 12, moment: '2016-01-04', userId: 3, user: { id: 3, username: 'common_user' }, distance: 4000, time: 240, averageSpeed: 1 },
          { id: 11, moment: '2016-01-03', userId: 3, user: { id: 3, username: 'common_user' }, distance: 3000, time: 180, averageSpeed: 1 },
          { id: 10, moment: '2016-01-02', userId: 3, user: { id: 3, username: 'common_user' }, distance: 2000, time: 120, averageSpeed: 1 },
          { id:  9, moment: '2016-01-01', userId: 3, user: { id: 3, username: 'common_user' }, distance: 1000, time: 60, averageSpeed: 1 },
        ]
      });
    });

    it('should be able to list self times via filter', function() {
      return utils.shouldGet(token, '/api/times?userId=3', {
        times: [
          { id: 12, moment: '2016-01-04', userId: 3, user: { id: 3, username: 'common_user' }, distance: 4000, time: 240, averageSpeed: 1 },
          { id: 11, moment: '2016-01-03', userId: 3, user: { id: 3, username: 'common_user' }, distance: 3000, time: 180, averageSpeed: 1 },
          { id: 10, moment: '2016-01-02', userId: 3, user: { id: 3, username: 'common_user' }, distance: 2000, time: 120, averageSpeed: 1 },
          { id:  9, moment: '2016-01-01', userId: 3, user: { id: 3, username: 'common_user' }, distance: 1000, time: 60, averageSpeed: 1 },
        ]
      });
    });

    it('should not be able to list times of other user', function() {
      return utils.shouldForbidGet(token, '/api/times?userId=2');
    });

    it('should be able to create time for self', function() {
      return utils.shouldPostCreate(token, '/api/times',
        { moment: '2016-02-01', distance: 10000, time: 300 },
        { time: { id: 13, moment: '2016-02-01', userId: 3, user: { id: 3, username: 'common_user' }, distance: 10000, time: 300, averageSpeed: 2 } }
      );
    });

    it('should not be able to create time for other user', function() {
      return utils.shouldForbidPost(token, '/api/times', { userId: 1, moment: '2016-02-01', distance: 10000, time: 300 });
    });

    it('should be able to read time from self', function() {
      return utils.shouldGet(token, '/api/times/9', {
        time: { id: 9, moment: '2016-01-01', userId: 3, user: { id: 3, username: 'common_user' }, distance: 1000, time: 60, averageSpeed: 1 }
      });
    });
    
    it('should not be able to read time from other user', function() {
      return utils.shouldForbidGet(token, '/api/times/1');
    });

    it('should be able to edit time from self', function() {
      return utils.shouldPut(token, '/api/times/9', 
        { distance: 2000, time: 240 },
        { time: { id: 9, moment: '2016-01-01', userId: 3, user: { id: 3, username: 'common_user' }, distance: 2000, time: 240, averageSpeed: 0.5 } }
      );
    });
    
    it('should not be able to edit time from other user', function() {
      return utils.shouldForbidPut(token, '/api/times/1', { distance: 2000, time: 240 });
    });

    it('should be able to delete time from self', function() {
      return utils.shouldDeleteAndThenNotFound(token, '/api/times/9');
    });
    
    it('should not be able to delete time from other user', function() {
      return utils.shouldForbidDelete(token, '/api/times/1');
    });
    
  });

});
