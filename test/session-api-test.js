var Promise = require('promise');
var request = require('supertest-as-promised');
var expect = require('expect.js');
var path = require('path');

var app = require(path.join(__dirname, '..', 'jogging-server'));

var utils = require(path.join(__dirname, 'utils'));
utils.config.app = app;

describe('/api/session', function() {

  beforeEach(utils.dbCreateAndSeed);

  it('unknown user should not be able to authenticate', function() {
    return request(app)
      .post('/api/session')
      .send({ username: 'unknown', password: 'unknown' })
      .expect(403)
      .expect({ error: 'forbidden', message: 'invalid_username_or_password' });
  });

  it('user should be able to authenticate', function() {
    return request(app)
      .post('/api/session')
      .send({ username: 'admin', password: 'secret' })
      .expect(200)
      .expect(function(res) {
        var body = res.body;
        expect(body.userId).to.be(1);
        expect(body.role).to.be('A');
        expect(body.token).to.be.ok();
        expect(body.username).to.be('admin');
      });
  });

  it('generated token should be valid to access other resources', function() {
    return Promise.resolve()
      .then(function() {
        // Sanity check: resource should not be accessible without token
        return request(app)
          .get('/api/times')
          .expect(401);
      })
      .then(function() {
        return request(app)
          .post('/api/session')
          .send({ username: 'admin', password: 'secret' })
          .expect(200)
          .then(function(res) {
            return res.body.token;
          });
      })
      .then(function(token) {
        return request(app)
          .get('/api/times')
          .set('Jogging-Token', token)
          .expect(200);
      });
  });

  it('user should be able to logout', function() {
    return Promise.resolve()
      .then(function() {
        // Authenticate
        return request(app)
          .post('/api/session')
          .send({ username: 'admin', password: 'secret' })
          .expect(200)
          .then(function(res) {
            return res.body.token;
          });
      })
      .then(function(token) {
        return request(app)
          .delete('/api/session')
          .set('Jogging-Token', token)
          .expect(204);
      });
  });

  it('token of a logged out user should not be valid', function() {
    return Promise.resolve()
      .then(function() {
        // Authenticate
        return request(app)
          .post('/api/session')
          .send({ username: 'admin', password: 'secret' })
          .expect(200)
          .then(function(res) {
            return res.body.token;
          });
      })
      .then(function(token) {
        // End session
        return request(app)
          .delete('/api/session')
          .set('Jogging-Token', token)
          .expect(204)
          .then(function() {
            return token;
          })
      })
      .then(function(token) {
        return request(app)
          .get('/api/times')
          .set('Jogging-Token', token)
          .expect(401, { error: 'unauthorized' });
      });
  });

});
