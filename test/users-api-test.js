var Promise = require('promise');
var request = require('supertest-as-promised');
var expect = require('expect.js');
var path = require('path');

var app = require(path.join(__dirname, '..', 'jogging-server'));

var utils = require(path.join(__dirname, 'utils'));
utils.config.app = app;

describe('/api/users', function() {

  beforeEach(utils.dbCreateAndSeed);

  describe('unauthorized', function() {

    it('should not be able to list users', function () {
      return request(app)
        .get('/api/users')
        .expect(401, { error: 'missing_header', missing: 'jogging-token' });
    });

    it('should not be able to read user', function () {
      return request(app)
        .get('/api/users/1')
        .expect(401, { error: 'missing_header', missing: 'jogging-token' });
    });

    it('should be able to create a common user', function () {
      return request(app)
        .post('/api/users')
        .send({ username: 'new_user', password: 'new_user_secret', passwordConfirmation: 'new_user_secret' })
        .expect(201, { user: { id: 4, username: 'new_user', role: 'C' } });
    });

    it('should not be able to update common user', function () {
      return request(app)
        .put('/api/users/3')
        .send({ username: 'x' })
        .expect(401, { error: 'missing_header', missing: 'jogging-token' });
    });

    it('should not be able to update user manager', function () {
      return request(app)
        .put('/api/users/2')
        .send({ username: 'x' })
        .expect(401, { error: 'missing_header', missing: 'jogging-token' });
    });

    it('should not be able to update admin user', function () {
      return request(app)
        .put('/api/users/1')
        .send({ username: 'x' })
        .expect(401, { error: 'missing_header', missing: 'jogging-token' });
    });

    it('should not be able to create user manager', function () {
      return request(app)
        .post('/api/users')
        .send({ username: 'new_user', password: 'new_user_secret', passwordConfirmation: 'new_user_secret', role: 'U' })
        .expect(401, { error: 'unauthorized' });
    });

    it('should not be able to create admin user', function () {
      return request(app)
        .post('/api/users')
        .send({ username: 'new_user', password: 'new_user_secret', passwordConfirmation: 'new_user_secret', role: 'A' })
        .expect(401, { error: 'unauthorized' });
    });

    it('should forbid delete user', function () {
      return request(app)
        .delete('/api/users/1')
        .expect(401, { error: 'missing_header', missing: 'jogging-token' });
    });

  });

  describe('admin', function() {

    var token;

    beforeEach(function() {
      return utils.authenticate('admin', 'secret').then(function(t) { token = t; });
    });

    it('should be able to list users', function() {
      return utils.shouldGet(token, '/api/users', {
        users: [
          { id: 1, username: 'admin',        role: 'A' },
          { id: 2, username: 'user_manager', role: 'U' },
          { id: 3, username: 'common_user',  role: 'C' }
        ]
      });
    });

    it('should be able to create admin user', function() {
      return Promise.resolve()
        .then(function() {
          // Sanity check: assert user does not exists (can't login)
          return utils.shouldForbidLogin('new_user', 'new_user_password');
        })
        .then(function() {
          // Create user
          return utils.shouldPostCreate(token, '/api/users',
            { username: 'new_user', password: 'new_user_password', passwordConfirmation: 'new_user_password', role: 'A' },
            { user: { id: 4, username: 'new_user', role: 'A' } }
          );
        })
        .then(function() {
          // Assert user shows on list
          return utils.shouldGet(token, '/api/users', {
            users: [
              { id: 1, username: 'admin',        role: 'A' },
              { id: 2, username: 'user_manager', role: 'U' },
              { id: 3, username: 'common_user',  role: 'C' },
              { id: 4, username: 'new_user',     role: 'A' }
            ]
          });
        })
        .then(function() {
          // Assert user can login
          return utils.shouldLogin('new_user', 'new_user_password');
        });
    });

    it('should be able to create user manager', function() {
      return Promise.resolve()
        .then(function() {
          // Sanity check: assert user does not exists (can't login)
          return utils.shouldForbidLogin('new_user', 'new_user_password');
        })
        .then(function() {
          // Create user
          return utils.shouldPostCreate(token, '/api/users',
            { username: 'new_user', password: 'new_user_password', passwordConfirmation: 'new_user_password', role: 'U' },
            { user: { id: 4, username: 'new_user', role: 'U' } }
          );
        })
        .then(function() {
          // Assert user shows on list
          return utils.shouldGet(token, '/api/users', {
            users: [
              { id: 1, username: 'admin',        role: 'A' },
              { id: 2, username: 'user_manager', role: 'U' },
              { id: 3, username: 'common_user',  role: 'C' },
              { id: 4, username: 'new_user',     role: 'U' }
            ]
          });
        })
        .then(function() {
          // Assert user can login
          return utils.shouldLogin('new_user', 'new_user_password');
        });
    });

    it('should be able to create common user', function() {
      return Promise.resolve()
        .then(function() {
          // Sanity check: assert user does not exists (can't login)
          return utils.shouldForbidLogin('new_user', 'new_user_password');
        })
        .then(function() {
          // Create user
          return utils.shouldPostCreate(token, '/api/users',
            { username: 'new_user', password: 'new_user_password', passwordConfirmation: 'new_user_password', role: 'C' },
            { user: { id: 4, username: 'new_user', role: 'C' } }
          );
        })
        .then(function() {
          // Assert user shows on list
          return utils.shouldGet(token, '/api/users', {
            users: [
              { id: 1, username: 'admin',        role: 'A' },
              { id: 2, username: 'user_manager', role: 'U' },
              { id: 3, username: 'common_user',  role: 'C' },
              { id: 4, username: 'new_user',     role: 'C' }
            ]
          });
        })
        .then(function() {
          // Assert user can login
          return utils.shouldLogin('new_user', 'new_user_password');
        });
    });

    it('should be able to read self user', function() {
      return utils.shouldGet(token, '/api/users/1', { user: { id: 1, username: 'admin', role: 'A' } });
    });

    it('should be able to read other user', function() {
      return utils.shouldGet(token, '/api/users/2', { user: { id: 2, username: 'user_manager', role: 'U' } });
    });

    it('should be able to update self password', function() {
      return utils.shouldUpdateSelfPassword(token, 1, 'admin', 'A');
    });

    it('should be able to update other user role', function() {
      return Promise.resolve()
        .then(function() {
          // Assert user shows as User Manager role
          return utils.shouldGet(token, '/api/users/2', { user: { id: 2, username: 'user_manager', role: 'U' } });
        })
        .then(function() {
          // Update user
          return utils.shouldPut(token, '/api/users/2', { role: 'A' }, { user: { id: 2, username: 'user_manager', role: 'A' } });
        })
        .then(function() {
          // Assert user can login as admin
          return utils.shouldLogin('user_manager', 'secret').then(function(res) {
            expect(res.body.role).to.be('A');
          });
        });
    });

    it('should not be able to update other user password', function() {
      return utils.shouldForbidPut(token, '/api/users/2', { password: 'new_secret', passwordConfirmation: 'new_secret' });
    });

    it('should not be able to update self role', function() {
      return utils.shouldForbidPut(token, '/api/users/1', { role: 'U' });
    });

    it('should be able to delete other user', function() {
      return Promise.resolve()
        .then(function() {
          // Sanity check: list all users
          return utils.shouldGet(token, '/api/users', {
            users: [
              { id: 1, username: 'admin',        role: 'A' },
              { id: 2, username: 'user_manager', role: 'U' },
              { id: 3, username: 'common_user',  role: 'C' }
            ]
          });
        })
        .then(function() {
          // Delete user
          return utils.shouldDeleteAndThenNotFound(token, '/api/users/2');
        })
        .then(function() {
          // Assert user does not appear on list anymore
          return utils.shouldGet(token, '/api/users', {
            users: [
              { id: 1, username: 'admin',       role: 'A' },
              { id: 3, username: 'common_user', role: 'C' }
            ]
          });
        })
        .then(function() {
          // Assert user can't login
          return utils.shouldForbidLogin('user_manager', 'secret');
        });
    });

    it('should not be able to delete self user', function() {
      return utils.shouldForbidDelete(token, '/api/users/1');
    });

  });

  describe('user manager', function() {

    var token;

    beforeEach(function() {
      return utils.authenticate('user_manager', 'secret').then(function(t) { token = t; });
    });

    it('should be able to list users', function() {
      return utils.shouldGet(token, '/api/users', {
        users: [
          { id: 1, username: 'admin',        role: 'A' },
          { id: 2, username: 'user_manager', role: 'U' },
          { id: 3, username: 'common_user',  role: 'C' }
        ]
      });
    });

    it('should not be able to create admin user', function() {
      return utils.shouldForbidPost(token, '/api/users', { username: 'new_user', password: 'bar', passwordConfirmation: 'bar', role: 'A' });
    });

    it('should be able to create user manager', function() {
      return Promise.resolve()
        .then(function() {
          // Sanity check: assert user does not exists (can't login)
          return utils.shouldForbidLogin('new_user', 'new_user_password');
        })
        .then(function() {
          // Create user
          return utils.shouldPostCreate(token, '/api/users',
            { username: 'new_user', password: 'new_user_password', passwordConfirmation: 'new_user_password', role: 'U' },
            { user: { id: 4, username: 'new_user', role: 'U' } }
          );
        })
        .then(function() {
          // Assert user shows on list
          return utils.shouldGet(token, '/api/users', {
            users: [
              { id: 1, username: 'admin',        role: 'A' },
              { id: 2, username: 'user_manager', role: 'U' },
              { id: 3, username: 'common_user',  role: 'C' },
              { id: 4, username: 'new_user',     role: 'U' }
            ]
          });
        })
        .then(function() {
          // Assert user can login
          return utils.shouldLogin('new_user', 'new_user_password');
        });
    });

    it('should be able to create common user', function() {
      return Promise.resolve()
        .then(function() {
          // Sanity check: assert user does not exists (can't login)
          return utils.shouldForbidLogin('new_user', 'new_user_password');
        })
        .then(function() {
          // Create user
          return utils.shouldPostCreate(token, '/api/users',
            { username: 'new_user', password: 'new_user_password', passwordConfirmation: 'new_user_password', role: 'C' },
            { user: { id: 4, username: 'new_user', role: 'C' } }
          );
        })
        .then(function() {
          // Assert user shows on list
          return utils.shouldGet(token, '/api/users', {
            users: [
              { id: 1, username: 'admin',        role: 'A' },
              { id: 2, username: 'user_manager', role: 'U' },
              { id: 3, username: 'common_user',  role: 'C' },
              { id: 4, username: 'new_user',     role: 'C' }
            ]
          });
        })
        .then(function() {
          // Assert user can login
          return utils.shouldLogin('new_user', 'new_user_password');
        });
    });

    it('should be able to read self user', function() {
      return utils.shouldGet(token, '/api/users/2', { user: { id: 2, username: 'user_manager', role: 'U' } });
    });

    it('should be able to read other user', function() {
      return utils.shouldGet(token, '/api/users/1', { user: { id: 1, username: 'admin', role: 'A' } });
    });

    it('should be able to update self password', function() {
      return utils.shouldUpdateSelfPassword(token, 2, 'user_manager', 'U');
    });

    it('should be able to update other user role to user manager', function() {
      return Promise.resolve()
        .then(function() {
          // Assert user shows as Common User role
          return utils.shouldGet(token, '/api/users/3', { user: { id: 3, username: 'common_user', role: 'C' } });
        })
        .then(function() {
          // Update user
          return utils.shouldPut(token, '/api/users/3', { role: 'U' }, { user: { id: 3, username: 'common_user', role: 'U' } });
        })
        .then(function() {
          // Assert user can login as user manager
          return utils.shouldLogin('common_user', 'secret').then(function(res) {
            expect(res.body.role).to.be('U');
          });
        });
    });

    it('should not be able to update other user password', function() {
      return utils.shouldForbidPut(token, '/api/users/3', { password: 'new_secret', passwordConfirmation: 'new_secret' });
    });

    it('should not be able to update other user role to admin', function() {
      return utils.shouldForbidPut(token, '/api/users/3', { role: 'A' });
    });

    it('should not be able to update self role', function() {
      return utils.shouldForbidPut(token, '/api/users/2', { role: 'C' });
    });

    it('should not be able to delete admin user', function() {
      return utils.shouldForbidDelete(token, '/api/users/1');
    });

    it('should be able to delete user', function() {
      return Promise.resolve()
        .then(function() {
          // Sanity check: list all users
          return utils.shouldGet(token, '/api/users', {
            users: [
              { id: 1, username: 'admin',        role: 'A' },
              { id: 2, username: 'user_manager', role: 'U' },
              { id: 3, username: 'common_user',  role: 'C' }
            ]
          });
        })
        .then(function() {
          // Delete user
          return utils.shouldDeleteAndThenNotFound(token, '/api/users/3');
        })
        .then(function() {
          // Assert user does not appear on list anymore
          return utils.shouldGet(token, '/api/users', {
            users: [
              { id: 1, username: 'admin',        role: 'A' },
              { id: 2, username: 'user_manager', role: 'U' }
            ]
          });
        })
        .then(function() {
          // Assert user can't login
          return utils.shouldForbidLogin('common_user', 'secret');
        });
    });

    it('should not be able to delete self', function() {
      return utils.shouldForbidDelete(token, '/api/users/2');
    });

  });

  describe('common user', function() {

    var token;

    beforeEach(function() {
      return utils.authenticate('common_user', 'secret').then(function(t) { token = t; });
    });

    it('should not be able to list users', function() {
      return utils.shouldForbidGet(token, '/api/users');
    });

    it('should not be able to read other user', function() {
      return utils.shouldForbidGet(token, '/api/users/1');
    });

    it('should be able to read self user', function() {
      return utils.shouldGet(token, '/api/users/3', { user: { id: 3, username: 'common_user', role: 'C' } });
    });

    it('should be able to update self password', function() {
      return utils.shouldUpdateSelfPassword(token, 3, 'common_user', 'C');
    });

    it('should not be able to update other user', function() {
      return utils.shouldForbidPut(token, '/api/users/2', { role: 'C' });
    });

    it('should not be able to delete self', function() {
      return utils.shouldForbidDelete(token, '/api/users/3');
    });

    it('should not be able to delete other', function() {
      return utils.shouldForbidDelete(token, '/api/users/1');
    });

  });

});
