var Promise = require('promise');
var request = require('supertest-as-promised');
var path    = require('path');

var dbCreate = require(path.join(__dirname, '..', 'scripts', 'db-create'));
var dbSeed   = require(path.join(__dirname, '..', 'scripts', 'db-seed'));

var config = {
  app: null
};

function dbCreateAndSeed(done) {
  dbCreate(function() {
    dbSeed(done);
  });
}

function authenticate(username, password) {
  return request(config.app)
    .post('/api/session')
    .send({ username: username, password: password })
    .expect(200)
    .then(function(res) {
      return res.body.token;
    });
}

function shouldLogin(username, password) {
  return request(config.app)
    .post('/api/session')
    .send({ username: username, password: password })
    .expect(200);
}

function shouldGet(token, url, body) {
  return request(config.app)
    .get(url)
    .set('Jogging-Token', token)
    .expect(200, body);
}

function shouldGetNotFound(token, url) {
  return request(config.app)
    .get(url)
    .set('Jogging-Token', token)
    .expect(404, { error: 'not_found' });
}

function shouldRequireToken(method, url, body) {
  var req = request(config.app)[method](url);
  if (body) {
    req = req.send(body);
  }
  return req.expect(401, { error: 'missing_header', missing: 'jogging-token' });
}

function shouldPut(token, url, requestBody, responseBody) {
  return request(config.app)
    .put(url)
    .set('Jogging-Token', token)
    .send(requestBody)
    .expect(200, responseBody);
}

function shouldDeleteAndThenNotFound(token, url) {
  return request(config.app)
    .delete(url)
    .set('Jogging-Token', token)
    .expect(204)
    .then(function() {
      return shouldGetNotFound(token, url);
    });
}

function shouldForbidDelete(token, url) {
  return request(config.app)
    .delete(url)
    .set('Jogging-Token', token)
    .expect(403, { error: 'forbidden' });
}

function shouldForbidLogin(username, password) {
  return request(config.app)
    .post('/api/session')
    .send({ username: username, password: password })
    .expect(403, { error: 'forbidden', message: 'invalid_username_or_password' });
}

function shouldForbidGet(token, url) {
  return request(config.app)
    .get(url)
    .set('Jogging-Token', token)
    .expect(403, { error: 'forbidden' });
}

function shouldForbidPost(token, url, body) {
  return request(config.app)
    .post(url)
    .set('Jogging-Token', token)
    .send(body)
    .expect(403, { error: 'forbidden' });
}

function shouldForbidPut(token, url, body) {
  return request(config.app)
    .put(url)
    .set('Jogging-Token', token)
    .send(body)
    .expect(403, { error: 'forbidden' });
}

function shouldPostCreate(token, url, requestBody, responseBody) {
  return request(config.app)
    .post(url)
    .set('Jogging-Token', token)
    .send(requestBody)
    .expect(201, responseBody);
}

function shouldUpdateSelfPassword(token, userId, username, role) {
  return Promise.resolve()
    .then(function() {
      // Assert can login with old password
      return shouldLogin(username, 'secret');
    })
    .then(function() {
      // Change password
      return shouldPut(token, '/api/users/' + userId,
        { password: 'new_secret', passwordConfirmation: 'new_secret' },
        { user: { id: userId, username: username, role: role } }
      );
    })
    .then(function() {
      // Assert can login with new password
      return shouldLogin(username, 'new_secret');
    })
    .then(function() {
      // Assert can't login with old password
      return shouldForbidLogin(username, 'secret');
    });
}

module.exports = {
  config: config,
  dbCreateAndSeed: dbCreateAndSeed,
  authenticate: authenticate,
  shouldLogin: shouldLogin,
  shouldGet: shouldGet,
  shouldGetNotFound: shouldGetNotFound,
  shouldRequireToken: shouldRequireToken,
  shouldPut: shouldPut,
  shouldDeleteAndThenNotFound: shouldDeleteAndThenNotFound,
  shouldForbidDelete: shouldForbidDelete,
  shouldForbidLogin: shouldForbidLogin,
  shouldForbidGet: shouldForbidGet,
  shouldForbidPost: shouldForbidPost,
  shouldForbidPut: shouldForbidPut,
  shouldPostCreate: shouldPostCreate,
  shouldUpdateSelfPassword: shouldUpdateSelfPassword
};
