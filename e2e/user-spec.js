var path = require('path');

var w = require(path.join(__dirname, 'utils'));

describe('Users', function() {

  it('should allow user creation by admin', function() {
    w.login('admin', 'secret');

    w.clickLink('Users');
    w.clickLink('New user');

    w.type('editUser.username', 'spec_test_username');
    w.type('editUser.password', 'little_secret');
    w.type('editUser.passwordConfirmation', 'little_secret');
    w.selectOption('editUser.role', 'Admin');
    w.clickButton('Create User');

    w.expectAlert('User created');

    w.logout();
  });

  it('should be able to login with created user', function() {
    w.login('spec_test_username', 'little_secret');
    w.clickLink('Users');
    w.logout();
  });

  it('should be able to change user role', function() {
    w.login('admin', 'secret');

    w.clickLink('Users');
    element(by.cssContainingText('tr', 'spec_test_username')).element(by.partialLinkText('Edit')).click();

    w.selectOption('editUser.role', 'Common User');
    w.clickButton('Edit User');

    w.expectAlert('User updated');

    w.logout();
  });

  it('updated user should not see users link anymore', function() {
    w.login('spec_test_username', 'little_secret');
    expect(element(by.className('navbar')).isElementPresent(by.partialLinkText('Users'))).toBe(false);
    w.logout();
  });

  it('created user should be able to change his password', function() {
    w.login('spec_test_username', 'little_secret');
    w.clickLink('spec_test_username');
    w.clickLink('Account');
    w.clickLink('Edit');
    w.type('editUser.password', 'new_secret');
    w.type('editUser.passwordConfirmation', 'new_secret');
    w.clickButton('Edit User');
    w.expectAlert('User updated');
    w.logout();

    w.visit('/');
    w.login('spec_test_username', 'new_secret');
    expect(element(by.partialLinkText('Times')).isPresent()).toBe(true);
    w.logout();
  });

  it('should be possible to delete the created user', function() {
    w.login('admin', 'secret');
    w.clickLink('Users');
    element(by.cssContainingText('tr', 'spec_test_username')).element(by.partialLinkText('Destroy')).click();
    w.clickButton('Confirm Destroy');
    w.expectAlert('User destroyed');
    w.logout();
  });

});
