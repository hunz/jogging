var path = require('path');
var w = require(path.join(__dirname, 'utils'));

describe('Signup', function() {

  afterAll(function() {
    w.destroyUser('new_test_spec_username');
  });

  it('should validate password and password confirmation at signup', function() {
    w.visit('/');
    w.clickLink('Sign up');
    w.type('signup.username', 'new_test_spec_username');
    w.type('signup.password', 'secret_signup');
    w.type('signup.passwordConfirmation', 'secret_signup2');
    w.clickButton('Sign up');
    w.expectAlert('Password and password confirmation does not match');
  });

  it('should accept sign up', function() {
    w.visit('/');
    w.clickLink('Sign up');
    w.type('signup.username', 'new_test_spec_username');
    w.type('signup.password', 'secret_signup');
    w.type('signup.passwordConfirmation', 'secret_signup');
    w.clickButton('Sign up');
    w.expectAlert('You have successfully created an account named "new_test_spec_username". You may login now.');
  });

  it('should accept login from signed up user', function() {
    w.login('new_test_spec_username', 'secret_signup');
    w.expectAlert('Welcome');
    w.logout();
  });

});

