var path = require('path');

var w = require(path.join(__dirname, 'utils'));

describe('Session', function() {

  it('should deny unknown user login', function() {
    w.login('unknown', 'unknown');
    w.expectAlert('Invalid username or password');
  });

  it('should accept login from admin', function() {
    w.login('admin', 'secret');
    w.expectAlert('Welcome, admin');
    w.logout();
    w.expectAlert('Good bye, admin');
  });

});
