var path = require('path');

var w = require(path.join(__dirname, 'utils'));

describe('Reports', function() {

  beforeAll(function() {
    w.setup();
  });

  afterAll(function() {
    w.teardown();
  });

  it('should show correct report', function() {
    // 2015-53
    addJoggingEntry('2016-01-01', '5.6', '1', '23');
    addJoggingEntry('2015-12-31', '4.7', '1', '09');
    
    // 2016-01
    addJoggingEntry('2016-01-04', '3.34', '0', '45');
    addJoggingEntry('2016-01-06', '8', '2', '3');
    addJoggingEntry('2016-01-10', '7.63', '1', '55');

    // 2016-02
    addJoggingEntry('2016-01-11', '6.4', '2', '10');

    w.clickLink('Reports');
    w.clickLink('Average speed and distance per week');
    w.clickButton('Execute');
    w.clickLink('Table');

    element.all(by.css('tr')).then(function(items) {
      expect(items.length).toBe(4);

      items[0].all(by.css('th')).then(function(data) {
        expect(data.length).toBe(3);
        expect(data[0].getText()).toBe('Week');
        expect(data[1].getText()).toBe('Total Distance');
        expect(data[2].getText()).toBe('Average Speed');
      });

      items[1].all(by.css('td')).then(function(data) {
        expect(data.length).toBe(3);
        expect(data[0].getText()).toBe('2015-53');
        expect(data[1].getText()).toBe('10.30km');
        expect(data[2].getText()).toBe('4.07km/h');
      });

      items[2].all(by.css('td')).then(function(data) {
        expect(data.length).toBe(3);
        expect(data[0].getText()).toBe('2016-01');
        expect(data[1].getText()).toBe('18.97km');
        expect(data[2].getText()).toBe('4.02km/h');
      });

      items[3].all(by.css('td')).then(function(data) {
        expect(data.length).toBe(3);
        expect(data[0].getText()).toBe('2016-02');
        expect(data[1].getText()).toBe('6.40km');
        expect(data[2].getText()).toBe('2.95km/h');
      });
    });
  });

});


function addJoggingEntry(date, distance, hour, minute) {
  w.clickLink('Times');

  element(by.className('glyphicon-plus')).click();

  element(by.model('editTime.moment')).clear();
  element(by.model('editTime.distance')).sendKeys(protractor.Key.BACK_SPACE);
  element(by.model('editTime.timeHour')).sendKeys(protractor.Key.BACK_SPACE);
  element(by.model('editTime.timeMinute')).sendKeys(protractor.Key.BACK_SPACE);

  w.type('editTime.moment', date);
  w.type('editTime.distance', distance);
  w.type('editTime.timeHour', hour);
  w.type('editTime.timeMinute', minute);

  w.clickButton('Create Time');
}

