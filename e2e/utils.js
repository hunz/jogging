function setup() {
  var time = new Date().getTime();
  var username = 'test_e2e_' + time;
  var password = 'secret';

  signup(username, password);
  login(username, password);
}

function teardown() {
  logout();
  destroyUser('test_e2e_');
}

function destroyUser(username) {
  login('admin', 'secret');
  clickLink('Users');
  element(by.cssContainingText('tr', username)).element(by.linkText('Destroy')).click();
  clickButton('Confirm Destroy');
  logout();
}

function signup(username, password) {
  visit('/');
  clickLink('Sign up');

  element(by.model('signup.username')).sendKeys(username);
  element(by.model('signup.password')).sendKeys(password);
  element(by.model('signup.passwordConfirmation')).sendKeys(password);
  element(by.buttonText('Sign up')).click();
}

function login(username, password) {
  visit('/');
  element(by.model('login.username')).sendKeys(username);
  element(by.model('login.password')).sendKeys(password);
  element(by.buttonText('Login')).click();
}

function logout() {
  element(by.className('navbar')).element(by.className('dropdown-toggle')).click();
  element(by.linkText('Logout')).click();
}

function expectAlert(text) {
  expect(element(by.className('alert')).getText()).toContain(text);
}

function clickLink(text) {
  element(by.partialLinkText(text)).click();
}

function clickButton(text) {
  element(by.partialButtonText(text)).click();
}

function selectOption(modelName, optionText) {
  element(by.model(modelName)).element(by.cssContainingText('option', optionText)).click();
}

function visit(path) {
  browser.get(path);
}

function type(modelName, text) {
  element(by.model(modelName)).sendKeys(text);
}

module.exports = {

  setup: setup,
  teardown: teardown,

  visit: visit,

  signup: signup,
  login: login,
  logout: logout,

  type: type,
  clickLink: clickLink,
  clickButton: clickButton,
  selectOption: selectOption,

  expectAlert: expectAlert,

  destroyUser: destroyUser

};
