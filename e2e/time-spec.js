var path = require('path');

var w = require(path.join(__dirname, 'utils'));

describe('Times', function() {

  beforeAll(function() {
    w.setup();
  });

  afterAll(function() {
    w.teardown();
  });

  it('should allow the new user to enter jogging times', function() {
    w.visit('/');

    element(by.className('glyphicon-plus')).click();

    element(by.model('editTime.moment')).clear();
    element(by.model('editTime.distance')).sendKeys(protractor.Key.BACK_SPACE);
    element(by.model('editTime.timeHour')).sendKeys(protractor.Key.BACK_SPACE);
    element(by.model('editTime.timeMinute')).sendKeys(protractor.Key.BACK_SPACE);

    w.type('editTime.moment', '2016-01-01');
    w.type('editTime.distance', '5.7');
    w.type('editTime.timeHour', '1');
    w.type('editTime.timeMinute', '15');

    expect(element(by.name('editTime-averageSpeed')).getAttribute('value')).toBe('4.56');
    expect(element(by.model('editTime.user')).isPresent()).toBe(false);

    w.clickButton('Create Time');
    w.expectAlert('Time entry created');
  });

  it('should show formatted values when listing', function() {
    w.visit('/');

    element.all(by.css('tr')).then(function(items) {
      expect(items.length).toBe(2);
      expect(items[0].getText()).toBe('Date Distance Time Average Speed');
      expect(items[1].getText()).toBe('Jan 1, 2016 5.70km 1h15m 4.56km/h Show Edit Destroy');
    });
  });

  it('should let user destroy time entry', function() {
    w.visit('/');

    element(by.cssContainingText('tr', 'Jan 1, 2016')).element(by.linkText('Destroy')).click();

    w.clickButton('Confirm Destroy');
    w.expectAlert('Time entry destroyed');

    w.visit('/');

    expect(element.all(by.css('tr')).count()).toBe(1);
  });

});

