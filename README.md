Jogging Time Tracker
====================

Track jogging time of users.


Features
--------

- User can create an account and log in.
- When logged in, user can see, edit and delete his times he entered.
- Three user levels: admin, user manager and common user.
- Each time entry when entered has a date, distance, and time.
- When displayed, each time entry has an average speed.
- Filter by dates from-to.
- Report on average speed & distance per week.
- REST API. It is possible to perform all user actions via the API, including authentication. Tests at folder `test`. See docs by navigating to `/api-docs`.
- Single page application, client-side uses AJAX for all actions.
- Infinite scroll on client side when listing time entries.


Demo
----

There is a demo at: http://jogging-time-tracker.heroku.com/

Example users:
  - username: admin / password: secret
  - username: user_manager / password: secret
  - username: thiago.negri / password: secret


Deploy to Heroku
----------------

In order to deploy to Heroku, run on project folder after checking out git repository:

    $ heroku apps:create
    $ heroku addons:create heroku-postgresql:hobby-dev
    $ heroku config:set NODE_ENV=prod
    $ git push heroku master
    $ heroku run npm run-script db-create

Done! If you want to populate with random data, run:

    $ heroku run npm run-script db-random-data

If you didn't populate with random data, connect to the database and create an admin user.

    $ heroku pg:psql
    => insert into users (username, password, role) values ('admin', md5('secret'), 'A');


Run locally (development)
-------------------------

Create a database and user for development and tests:

    => create role jogging_user login password 'secret';
    => create database jogging;
    => grant all on database jogging to jogging_user;

    => create role jogging_user_test login password 'secret_test';
    => create database jogging_test;
    => grant all on database jogging_test to jogging_user_test;

Install dependencies:

    $ npm install

Start the server:

    $ npm start

Server process will watch file changes and restart automatically to fetch code changes.


Run tests
---------


### Functional tests

To run the tests:

    $ npm test

Make sure you have created the database and user for running tests - see "Run locally (development)".

The output should look like this:

    /api/reports
      /average-speed-and-distance-per-week
        ✓ should not be accessible by unauthorized request (51ms)
        ✓ should be accessible by admin (83ms)
        ✓ should be accessible by user manager (41ms)
        ✓ should be accessible by common user (42ms)

    /api/session
      ✓ unknown user should not be able to authenticate
      ✓ user should be able to authenticate
      ✓ generated token should be valid to access other resources (52ms)
      ✓ user should be able to logout (46ms)
      ✓ token of a logged out user should not be valid (74ms)

    /api/times
      unauthorized
        ✓ should not be able to list times
        ✓ should not be able to create time
        ✓ should not be able to read time
        ✓ should not be able to update time
        ✓ should not be able to delete time
      admin
        ✓ should see times of all users when listing
        ✓ should be able to list times of self
        ✓ should be able to list times of other user
        ✓ should be able to create time for self
        ✓ should be able to create time for other user
        ✓ should be able to read time from self
        ✓ should be able to read time from other user
        ✓ should be able to edit time from self
        ✓ should be able to edit time from other user
        ✓ should be able to delete time from self
        ✓ should be able to delete time from other user
      user manager
        ✓ should only see self times when listing
        ✓ should be able to list self times
        ✓ should not be able to list times of admin
        ✓ should not be able to list times of other user
        ✓ should be able to create time for self
        ✓ should not be able to create time for other user
        ✓ should be able to read time from self
        ✓ should not be able to read time from other user
        ✓ should be able to edit time from self
        ✓ should not be able to edit time from other user
        ✓ should be able to delete time from self
        ✓ should not be able to delete time from other user
      common user
        ✓ should only see self times when listing
        ✓ should be able to list self times via filter
        ✓ should not be able to list times of other user
        ✓ should be able to create time for self
        ✓ should not be able to create time for other user
        ✓ should be able to read time from self
        ✓ should not be able to read time from other user
        ✓ should be able to edit time from self
        ✓ should not be able to edit time from other user
        ✓ should be able to delete time from self (64ms)
        ✓ should not be able to delete time from other user

    /api/users
      unauthorized
        ✓ should not be able to list users
        ✓ should not be able to read user
        ✓ should be able to create a common user
        ✓ should not be able to update common user
        ✓ should not be able to update user manager
        ✓ should not be able to update admin user
        ✓ should not be able to create user manager
        ✓ should not be able to create admin user
        ✓ should forbid delete user
      admin
        ✓ should be able to list users
        ✓ should be able to create admin user (55ms)
        ✓ should be able to create user manager (44ms)
        ✓ should be able to create common user (44ms)
        ✓ should be able to read self user
        ✓ should be able to read other user
        ✓ should be able to update self password
        ✓ should be able to update other user role (136ms)
        ✓ should not be able to update other user password
        ✓ should not be able to update self role
        ✓ should be able to delete other user (60ms)
        ✓ should not be able to delete self user
      user manager
        ✓ should be able to list users
        ✓ should not be able to create admin user
        ✓ should be able to create user manager (43ms)
        ✓ should be able to create common user (43ms)
        ✓ should be able to read self user
        ✓ should be able to read other user
        ✓ should be able to update self password (71ms)
        ✓ should be able to update other user role to user manager (56ms)
        ✓ should not be able to update other user password
        ✓ should not be able to update other user role to admin
        ✓ should not be able to update self role
        ✓ should not be able to delete admin user
        ✓ should be able to delete user (65ms)
        ✓ should not be able to delete self
      common user
        ✓ should not be able to list users
        ✓ should not be able to read other user
        ✓ should be able to read self user
        ✓ should be able to update self password (155ms)
        ✓ should not be able to update other user
        ✓ should not be able to delete self
        ✓ should not be able to delete other


    90 passing (53s)

The tests goes through the REST API all the way to read and write to the database.
Some tests are reported as slow because of that.

As the entire suite is quite fast (less than one minute), I think the benefits of
testing with a real database outweights the performance gain possible by mocking
the persistence layer.


### End to end tests

#### Dependencies

Make sure you have protractor installed and with available web drivers:

    $ npm install --global protractor
    $ webdriver-manager update

#### Running

Start our server connecting to test database:

    $ NODE_ENV=test npm start

Make sure test database is on seed state:

    $ NODE_ENV=test npm run db-create
    $ NODE_ENV=test npm run db-seed

Start webdriver-manager:

    $ webdriver-manager start

Run the tests:

    $ npm run e2e-test

Done.

To make it a bit easier, you may run `run_e2e_tests.sh`. This script will
make sure the database is in seed state before running the tests.

Running the script should output something like this:

    $ ./run_e2e_tests.sh

    > jogging@0.0.0 db-create /home/tnegri/jogging
    > node scripts/db-create.js


    > jogging@0.0.0 db-seed /home/tnegri/jogging
    > node scripts/db-seed.js


    > jogging@0.0.0 e2e-test /home/tnegri/jogging
    > protractor protractor-conf.js

    [23:44:54] I/hosted - Using the selenium server at http://localhost:4444/wd/hub
    [23:44:54] I/launcher - Running 1 instances of WebDriver
    Started
    ...............


    15 specs, 0 failures
    Finished in 39.448 seconds
    [23:45:34] I/launcher - 0 instance(s) of WebDriver still running
    [23:45:34] I/launcher - chrome #01 passed


How the code is organized
-------------------------

- Client code (single page application using AngularJS) is in folder `client`.
- Server code (REST API using ExpressJS) is in folder `server`.
- Functional tests are in folder `test`.
- End to end tests are in folder `e2e`.
- Server start up code is in folder `bin`.

### Flow example

User opens up the browser and points to our server address. ExpressJS will serve
the `client/public/index.html` file, which contains our single page application.

The single page application will establish its internal routing ([client/.../routes-config.js](client/public/javascripts/config/routes-config.js))
and redirect the user to the login view ([client/.../login.html](client/public/views/login.html)).

When user logs in, [client/.../login-controller.js](client/public/javascripts/controllers/login-controller.js)
will request the login to [client/.../session-service.js](client/public/javascripts/services/session-service.js),
which in turn will issue an HTTP request to the server process to generate a new session for the user.
On the server side, this will hit [server/.../session-api.js](server/routes/api/session-api.js),
which will validate the request content and delegate to 
[server/.../session-controller.js](server/controllers/session-controller.js) to handle the session
creation. The controller will manipulate the persistence layer via
[server/models/*.js](server/models) to find out if the user is allowed access or not,
generating an appropriate response for the HTTP request.

After completing the HTTP request, the single page application will define it's
current session variables, redirect to the appropriate application route. Each
subsequent request made to the server will contain the header `Jogging-Token`
with the token obtained from the login action.
[client/.../token-interceptor.js](client/public/javascripts/interceptors/token-interceptor.js) is responsible for this.

### Client

- Client entry point is defined in [client/public/index.html](client/public/index.html).
- All client views are defined in [client/public/views/*.html](client/public/views).
- All JavaScript (Angular modules, controllers, routes, etc) is defined in [client/public/javascripts/**/*.js](client/public/javascripts).
   - Routes are defined in [client/public/javascripts/config/routes-config.js](client/public/javascripts/config/routes-config.js).
   - Controllers are defined in [client/public/javascripts/controllers/*.js](client/public/javascripts/controllers).
   - Filters are defined in [client/public/javascripts/filters/*.js](client/public/javascripts/filters).
   - Interceptors are defined in [client/public/javascripts/interceptors/*.js](client/public/javascripts/interceptors).
   - Event listeners are defined in [client/public/javascripts/listeners/*.js](client/public/javascripts/listeners).
   - Services are defined in [client/public/javascripts/services/*.js](client/public/javascripts/services).
- All styles are defined in [client/public/stylesheets/style.css](client/public/stylesheets/style.css).
- Swagger UI resides in [client/swagger-ui](client/swagger-ui).
   - It is customized to point to our REST API definition file (see below) and
     to remove the header banner.
- Swagger REST API definition is in [client/public/api.json](client/public/api.json).

### Server

- ExpressJS application is defined in [jogging-server.js](jogging-server.js)
- Environment is defined in [server/config/environment.js](server/config/environment.js)
- Routes (REST API endpoints) are defined in [server/routes/api/*.js](server/routes/api)
- Serving the Swagger REST API documentation is defined in [server/routes/api-docs.js](server/routes/api-docs.js)
- Middlewares (e.g. authentication, contract validator) are defined in [server/middlewares/*.js](server/middlewares)
- Controllers (action handlers, business logic) are defined in [server/controllers/*.js](server/controllers)
- Models (persistence layer) are defined in [server/models/*.js](server/models)

