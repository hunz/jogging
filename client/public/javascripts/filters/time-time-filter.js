angular.module('jogging').filter(
  'timeTime',
  function() {
    return function(timeTime) {
      var minute = timeTime % 60;
      var hour = (timeTime - minute) / 60;
      if (minute < 10) {
        minute = '0' + minute;
      }
      return hour + 'h' + minute + 'm';
    };
  });