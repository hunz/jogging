angular.module('jogging').filter(
  'userRole',
  function() {
    return function(role) {
      switch (role) {
        case 'A':
          return 'Admin';
        case 'U':
          return 'User Manager';
        case 'C':
          return 'User';
      }
    }
  });