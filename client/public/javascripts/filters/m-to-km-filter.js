angular.module('jogging').filter(
  'mToKm',
  function() {
    return function(value) {
      return (value / 1000).toFixed(2);
    };
  });