angular.module('jogging').service(
  'TimesService',
  function($http, $httpParamSerializer) {
    this.list = function(params) {
      var url = '/api/times?' + $httpParamSerializer(params);
      return $http.get(url);
    };
    this.read = function(id) {
      var url = '/api/times/' + encodeURIComponent(id);
      return $http.get(url);
    };
    this.create = function(time) {
      var url = '/api/times';
      return $http.post(url, time);
    };
    this.update = function(id, time) {
      var url = '/api/times/' + encodeURIComponent(id);
      return $http.put(url, time);
    };
    this.destroy = function(id) {
      var url = '/api/times/' + encodeURIComponent(id);
      return $http.delete(url);
    };
  });