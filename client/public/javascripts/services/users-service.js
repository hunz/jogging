angular.module('jogging').service(
  'UsersService',
  function($http) {
    this.list = function() {
      var url = '/api/users';
      return $http.get(url);
    };
    this.read = function(id) {
      var url = '/api/users/' + encodeURIComponent(id);
      return $http.get(url);
    };
    this.create = function(user) {
      var url = '/api/users';
      return $http.post(url, user);
    };
    this.update = function(id, user) {
      var url = '/api/users/' + encodeURIComponent(id);
      return $http.put(url, user);
    };
    this.destroy = function(id) {
      var url = '/api/users/' + encodeURIComponent(id);
      return $http.delete(url);
    };
    this.search = function(query) {
      var url = '/api/users?search=' + encodeURIComponent(query);
      return $http.get(url);
    };
  });