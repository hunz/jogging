angular.module('jogging').service(
  'StorageService',
  function() {
    this.setSession = function(newSession) {
      localStorage['username'] = newSession.username;
      localStorage['token'] = newSession.token;
      localStorage['userId'] = newSession.userId;
      localStorage['role'] = newSession.role;
    };
    this.removeSession = function() {
      delete localStorage['token'];
      delete localStorage['username'];
      delete localStorage['userId'];
      delete localStorage['role'];
    };
    this.getToken = function() {
      return localStorage['token'];
    };
    this.getUser = function() {
      return {
        id: this.getUserId(),
        username: this.getUsername()
      };
    };
    this.getUsername = function() {
      return localStorage['username'];
    };
    this.getUserId = function() {
      return localStorage['userId'];
    };
    this.getRole = function() {
      return localStorage['role'];
    };
    this.isAdmin = function() {
      return this.getRole() == 'A';
    };
    this.isUserManager = function() {
      return this.getRole() == 'U';
    };
    this.hasToken = function() {
      return !!localStorage['token'];
    };
    this.flash = function(newFlash) {
      var oldFlash = sessionStorage['flash'];

      if (newFlash) {
        sessionStorage['flash'] = newFlash;
      } else {
        delete sessionStorage['flash'];
      }

      if (typeof oldFlash !== 'undefined') {
        return oldFlash;
      } else {
        return false;
      }
    };
  });