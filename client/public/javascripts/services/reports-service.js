angular.module('jogging').service(
  'ReportsService',
  function($http, $httpParamSerializer) {
    this.averageSpeedAndDistancePerWeek = function(params) {
      var qs = $httpParamSerializer(params);
      var url = '/api/reports/average-speed-and-distance-per-week?' + qs;
      return $http.get(url);
    };
  });