angular.module('jogging').service(
  'SessionService',
  function($http, StorageService) {
    this.login = function(username, password) {
      return $http.post('/api/session', {
        username: username,
        password: password
      }).then(function(response) {
        StorageService.setSession(response.data);
        return true;
      }, function(response) {
        if (response.data &&
          response.data.error &&
          response.data.error === 'forbidden' &&
          response.data.message === 'invalid_username_or_password') {
          return false;
        }
        throw response;
      });
    };

    this.logout = function() {
      return $http.delete('/api/session').finally(function() {
        StorageService.removeSession();
      });
    };
  });