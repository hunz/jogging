angular.module('jogging').config(
  function($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise('/times');

    $stateProvider
      .state('signup', {
        url: '/signup',
        templateUrl: 'views/signup.html',
        controller: 'SignupController',
        controllerAs: 'signup',
        data: {
          public: true
        }
      })
      .state('login', {
        url: '/login',
        templateUrl: 'views/login.html',
        controller: 'LoginController',
        controllerAs: 'login',
        data: {
          public: true
        }
      })
      .state('app', {
        abstract: true,
        url: '/',
        templateUrl: 'views/home.html',
        controller: 'HomeController',
        controllerAs: 'home'
      })
      .state('app.notFound', {
        url: 'not-found',
        templateUrl: 'views/404.html'
      })
      .state('app.forbidden', {
        url: 'forbidden',
        templateUrl: 'views/403.html'
      })
      .state('app.listUsers', {
        url: 'users',
        templateUrl: 'views/list-users.html',
        controller: 'ListUsersController',
        controllerAs: 'listUsers',
        resolve: {
          users: function($state, UsersService) {
            return UsersService.list().then(function(response) {
              return response.data.users;
            });
          }
        },
        data: {
          roles: ['U', 'A']
        }
      })
      .state('app.newUser', {
        url: 'users/new',
        templateUrl: 'views/edit-user.html',
        controller: 'EditUserController',
        controllerAs: 'editUser',
        resolve: {
          user: function() {
            return {
              isNew: true
            };
          }
        },
        data: {
          roles: ['U', 'A']
        }
      })
      .state('app.showUser', {
        url: 'users/:id',
        templateUrl: 'views/show-user.html',
        controller: 'ShowUserController',
        controllerAs: 'showUser',
        resolve: {
          user: function($stateParams, $state, UsersService) {
            return UsersService.read($stateParams.id).then(function(response) {
              return response.data.user;
            });
          }
        }
      })
      .state('app.editUser', {
        url: 'users/:id/edit',
        templateUrl: 'views/edit-user.html',
        controller: 'EditUserController',
        controllerAs: 'editUser',
        resolve: {
          user: function($stateParams, $state, UsersService) {
            return UsersService.read($stateParams.id).then(function(response) {
              return response.data.user;
            });
          }
        }
      })
      .state('app.destroyUser', {
        url: 'users/:id/destroy',
        templateUrl: 'views/destroy-user.html',
        controller: 'DestroyUserController',
        controllerAs: 'destroyUser',
        resolve: {
          user: function($stateParams, $state, UsersService) {
            return UsersService.read($stateParams.id).then(function(response) {
              return response.data.user;
            });
          }
        },
        data: {
          roles: ['U', 'A']
        }
      })
      .state('app.listTimes', {
        url: 'times',
        templateUrl: 'views/list-times.html',
        controller: 'ListTimesController',
        controllerAs: 'listTimes',
        resolve: {
          times: function(TimesService) {
            return TimesService.list().then(function(response) {
              return response.data.times;
            });
          }
        }
      })
      .state('app.newTime', {
        url: 'times/new',
        templateUrl: 'views/edit-time.html',
        controller: 'EditTimeController',
        controllerAs: 'editTime',
        resolve: {
          time: function() {
            return {
              isNew: true,
              moment: moment().toISOString().substring(0, 10),
              distance: 0,
              time: 0
            };
          }
        }
      })
      .state('app.showTime', {
        url: 'times/:id',
        templateUrl: 'views/show-time.html',
        controller: 'ShowTimeController',
        controllerAs: 'showTime',
        resolve: {
          time: function($stateParams, $state, TimesService) {
            return TimesService.read($stateParams.id).then(function(response) {
              return response.data.time;
            });
          }
        }
      })
      .state('app.editTime', {
        url: 'times/:id/edit',
        templateUrl: 'views/edit-time.html',
        controller: 'EditTimeController',
        controllerAs: 'editTime',
        resolve: {
          time: function($stateParams, $state, TimesService) {
            return TimesService.read($stateParams.id).then(function(response) {
              return response.data.time;
            });
          }
        }
      })
      .state('app.destroyTime', {
        url: 'times/:id/destroy',
        templateUrl: 'views/destroy-time.html',
        controller: 'DestroyTimeController',
        controllerAs: 'destroyTime',
        resolve: {
          time: function($stateParams, TimesService) {
            return TimesService.read($stateParams.id).then(function(response) {
              return response.data.time;
            });
          }
        }
      })
      .state('app.listReports', {
        url: 'reports',
        templateUrl: 'views/list-reports.html'
      })
      .state('app.reportAverageSpeedAndDistancePerWeek', {
        url: 'reports/average-speed-and-distance-per-week',
        templateUrl: 'views/report-average-speed-and-distance-per-week.html',
        controller: 'ReportAverageSpeedAndDistancePerWeekController',
        controllerAs: 'reportAverageSpeedAndDistancePerWeek'
      });
  });