angular.module('jogging').run(
  function($rootScope) {
    $rootScope.$on('$stateChangeStart', function() {
      NProgress.start();
    });
    $rootScope.$on('$stateChangeSuccess', function() {
      NProgress.done();
    });
    $rootScope.$on('$stateChangeError', function() {
      NProgress.done();
    });
  });