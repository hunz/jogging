angular.module('jogging').run(
  function($rootScope, $state, StorageService) {
    $rootScope.$on('$stateChangeError', function(e, toState, toParams, fromState, fromParams, error) {
      if (error && error.status) {
        switch (error.status) {
          case 404:
            $state.go('app.notFound', null, {
              location: false
            });
            e.preventDefault();
            break;

          case 403:
            if (StorageService.hasToken()) {
              $state.go('app.forbidden', null, {
                location: false
              });
              e.preventDefault();
            } else {
              $state.go('login');
              e.preventDefault();
            }
            break;

          case 401:
            $state.go('login');
            e.preventDefault();
            break;
        }
      }
    });
  }
);