angular.module('jogging').controller(
  'EditUserController',
  function($state, user, StorageService, UsersService) {
    var vm = this;

    vm.flash = StorageService.flash();
    vm.isNew = user.isNew || false;
    vm.id = user.id;
    vm.username = user.username;
    vm.password = '';
    vm.passwordConfirmation = '';
    vm.role = user.role;
    vm.loading = false;

    vm.showRole = StorageService.getUserId() != vm.id;
    vm.showPassword = vm.isNew || StorageService.getUserId() == vm.id;

    vm.submit = function() {
      var changes = {};

      if (vm.username) {
        changes.username = vm.username;
      }

      if (vm.role) {
        changes.role = vm.role;
      }

      if (vm.password || vm.passwordConfirmation) {
        if (vm.password !== vm.passwordConfirmation) {
          vm.flash = 'Password and password confirmation does not match.';
          return;
        }
        changes.password = vm.password;
        changes.passwordConfirmation = vm.passwordConfirmation;
      }

      if (vm.isNew) {
        vm.loading = true;
        UsersService.create(changes).then(function(response) {
          StorageService.flash('User created.');
          $state.go('app.showUser', {
            id: response.data.user.id
          });
        }, function() {
          vm.loading = false;
        });
      } else {
        vm.loading = true;
        UsersService.update(vm.id, changes).then(function(response) {
          StorageService.flash('User updated.');
          $state.go('app.showUser', {
            id: response.data.user.id
          });
        }, function() {
          vm.loading = false;
        });
      }
    };
  });