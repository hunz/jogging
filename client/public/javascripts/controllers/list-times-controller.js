angular.module('jogging').controller(
  'ListTimesController',
  function(times, StorageService, UsersService, TimesService) {
    var vm = this;

    vm.flash = StorageService.flash();
    vm.filterFrom = '';
    vm.filterTo = '';
    vm.showUserColumn = StorageService.isAdmin();
    vm.times = times;
    vm.hasNextPage = times && times.length > 0;
    vm.lastParams = {};
    vm.loadingPage = false;
    vm.loading = false;

    vm.searchUsers = function(query) {
      return UsersService.search(query).then(function(response) {
        return response.data.users;
      });
    };

    vm.filter = function() {
      vm.lastParams = {};

      if (vm.filterFrom) {
        vm.lastParams.from = vm.filterFrom;
      }

      if (vm.filterTo) {
        vm.lastParams.to = vm.filterTo;
      }

      if (vm.filterUser && vm.filterUser.id) {
        vm.lastParams.userId = vm.filterUser.id;
      }

      vm.loading = true;
      TimesService.list(vm.lastParams).then(function(response) {
        vm.times = response.data.times;
        vm.hasNextPage = vm.times && vm.times.length > 0;
      }).finally(function() {
        vm.loading = false;
      });
    };

    vm.nextPage = function() {
      if (vm.times.length < 1) {
        return;
      }

      vm.lastParams.lastSeenId = vm.times[vm.times.length - 1].id;

      vm.loadingPage = true;
      TimesService.list(vm.lastParams).then(function(response) {
        if (response.data.times.length > 0) {
          vm.times = vm.times.concat(response.data.times);
          vm.hasNextPage = true;
        } else {
          vm.hasNextPage = false;
        }
      }).finally(function() {
        vm.loadingPage = false;
      });
    };
  });