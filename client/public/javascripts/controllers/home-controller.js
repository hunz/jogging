angular.module('jogging').controller(
  'HomeController',
  function($scope, $state, StorageService, SessionService) {
    var vm = this;

    vm.flash = StorageService.flash();
    vm.username = StorageService.getUsername();
    vm.showListUsersLink = StorageService.isAdmin() || StorageService.isUserManager();
    vm.userId = StorageService.getUserId();
    vm.loading = false;

    $scope.$on('$stateChangeStart', function() {
      vm.flash = '';
    });

    $scope.$on('http.error', function(e, response) {
      if (response.status === -1) { // XHR cancelled or couldn't connect
        vm.flash = 'Oops.. Something bad happened. ' +
          'We could not contact the server. Try again in a few minutes.';
      }
    });

    vm.logout = function() {
      vm.loading = true;

      SessionService.logout().then(function() {
        StorageService.flash('Good bye, ' + vm.username + '.');
        $state.go('login');
      }, function() {
        vm.loading = false;
      });
    };
  });
