angular.module('jogging').controller(
  'SignupController',
  function($state, $timeout, StorageService, UsersService) {
    var vm = this;

    vm.flash = StorageService.flash();
    vm.username = '';
    vm.password = '';
    vm.passwordConfirmation = '';
    vm.loading = false;

    vm.submit = function() {
      var user = {
        username: vm.username,
        password: vm.password,
        passwordConfirmation: vm.passwordConfirmation
      };

      if (user.password !== user.passwordConfirmation) {
        vm.flash = 'Password and password confirmation does not match.';
        return;
      }

      vm.loading = true;
      vm.flash = '';

      UsersService.create(user).then(function(success) {
        var msg;
        if (success) {
          msg = 'You have successfully created an account named "' +
            user.username + '". You may login now.';
          StorageService.flash(msg);
          $state.go('login');
        } else {
          vm.flash = 'Invalid username or password';
          vm.loading = false;
          $timeout(function() {
            document.getElementsByClassName('js-signup-username')[0].focus();
          });
        }
      }, function() {
        vm.loading = false;
      });
    };
  });