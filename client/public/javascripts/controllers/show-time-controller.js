angular.module('jogging').controller(
  'ShowTimeController',
  function(time, StorageService) {
    var vm = this;
    vm.flash = StorageService.flash();
    vm.id = time.id;
    vm.moment = time.moment;
    vm.distance = time.distance;
    vm.time = time.time;
    vm.averageSpeed = time.averageSpeed;
    if (time.user) {
      vm.username = time.user.username;
    }
  });