angular.module('jogging').controller(
  'EditTimeController',
  function($scope, $state, time, StorageService, UsersService, TimesService) {
    var vm = this;
    var timeTime = time.time || 0;
    var minute = timeTime % 60;
    var hour = (timeTime - minute) / 60;

    vm.flash = StorageService.flash();
    vm.isNew = time.isNew || false;
    vm.id = time.id;
    vm.moment = moment(time.moment).toDate();
    vm.distance = (time.distance / 1000) || 0;
    vm.timeHour = hour;
    vm.timeMinute = minute;
    vm.user = time.user || StorageService.getUser();
    vm.showUser = StorageService.isAdmin() || StorageService.isUserManager();

    vm.averageSpeed = parseFloat(vm.distance) / (parseInt(vm.timeHour, 10) + parseInt(vm.timeMinute, 10) / 60);

    vm.loading = false;

    $scope.$watchCollection(function() {
      return [vm.distance, vm.timeHour, vm.timeMinute];
    }, function() {
      vm.averageSpeed = parseFloat(vm.distance) / (parseInt(vm.timeHour, 10) + parseInt(vm.timeMinute, 10) / 60);
    });

    vm.searchUsers = function(query) {
      return UsersService.search(query).then(function(response) {
        return response.data.users;
      });
    };

    vm.submit = function() {
      var time;
      var parsedMoment;
      var parsedDistance;
      var parsedTimeHour;
      var parsedTimeMinute;

      parsedMoment = moment(vm.moment);
      if (!parsedMoment.isValid()) {
        vm.flash = 'Invalid date format.';
        return;
      }

      parsedDistance = parseFloat(vm.distance);
      if (isNaN(parsedDistance) || parsedDistance < 0) {
        vm.flash = 'Invalid distance.';
        return;
      }

      parsedTimeHour = parseInt(vm.timeHour, 10);
      if (isNaN(parsedTimeHour) || parsedTimeHour < 0) {
        vm.flash = 'Invalid time (hour).';
        return;
      }

      parsedTimeMinute = parseInt(vm.timeMinute, 10);
      if (isNaN(parsedTimeMinute) || parsedTimeMinute < 0) {
        vm.flash = 'Invalid time (minute).';
        return;
      }

      vm.flash = '';

      time = {
        moment: parsedMoment.toISOString().substring(0, 10),
        distance: (parsedDistance * 1000).toFixed(0),
        time: parsedTimeHour * 60 + parsedTimeMinute
      };

      if (StorageService.isAdmin()) {
        time.userId = vm.user.id;
      }

      if (vm.isNew) {
        vm.loading = true;
        TimesService.create(time).then(function(response) {
          StorageService.flash('Time entry created.');
          $state.go('app.showTime', {
            id: response.data.time.id
          });
        }, function() {
          vm.loading = false;
        });
      } else {
        vm.loading = true;
        TimesService.update(vm.id, time).then(function(response) {
          StorageService.flash('Time entry updated.');
          $state.go('app.showTime', {
            id: response.data.time.id
          });
        }, function() {
          vm.loading = false;
        });
      }
    };
  });