angular.module('jogging').controller(
  'ListUsersController',
  function(users, StorageService) {
    var vm = this;
    vm.flash = StorageService.flash();
    vm.users = users;
  });