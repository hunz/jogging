angular.module('jogging').controller(
  'ShowUserController',
  function(user, StorageService) {
    var vm = this;
    vm.flash = StorageService.flash();
    vm.id = user.id;
    vm.username = user.username;
    vm.role = user.role;
    vm.showDestroy = StorageService.isAdmin() || StorageService.isUserManager();
  });