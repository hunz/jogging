angular.module('jogging').controller(
  'DestroyUserController',
  function($state, user, StorageService, UsersService) {
    var vm = this;

    vm.flash = StorageService.flash();
    vm.id = user.id;
    vm.username = user.username;
    vm.role = user.role;
    vm.loading = false;

    vm.submit = function() {
      vm.loading = true;
      UsersService.destroy(vm.id).then(function(response) {
        StorageService.flash('User destroyed.');
        $state.go('app.listUsers');
      }, function() {
        vm.loading = false;
      });
    };
  });