angular.module('jogging').controller(
  'LoginController',
  function($scope, $state, $timeout, StorageService, SessionService) {
    var vm = this;

    vm.flash = StorageService.flash();
    vm.username = '';
    vm.password = '';
    vm.loading = false;

    vm.submit = function() {
      vm.loading = true;
      vm.flash = '';

      SessionService.login(vm.username, vm.password).then(function(success) {
        if (success) {
          StorageService.flash('Welcome, ' + vm.username + '.');
          $state.go('app.listTimes');
        } else {
          vm.flash = 'Invalid username or password';
          vm.loading = false;
          $timeout(function() {
            document.getElementsByClassName('js-login-username')[0].focus();
          });
        }
      }, function() {
        vm.loading = false;
      });
    };
  });