angular.module('jogging').controller(
  'DestroyTimeController',
  function($state, time, StorageService, TimesService) {
    var vm = this;
    vm.flash = StorageService.flash();
    vm.id = time.id;
    vm.moment = time.moment;
    vm.distance = time.distance;
    vm.time = time.time;
    vm.averageSpeed = time.averageSpeed;
    if (time.user) {
      vm.username = time.user.username;
    }
    vm.loading = false;
    vm.submit = function() {
      vm.loading = true;
      TimesService.destroy(time.id).then(function(response) {
        StorageService.flash('Time entry destroyed.');
        $state.go('app.listTimes');
      }, function() {
        vm.loading = false;
      });
    };
  });