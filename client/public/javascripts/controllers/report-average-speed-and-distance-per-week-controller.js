angular.module('jogging').controller(
  'ReportAverageSpeedAndDistancePerWeekController',
  function(StorageService, UsersService, ReportsService) {
    var vm = this;

    vm.flash = StorageService.flash();
    vm.to = '';
    vm.from = '';
    vm.data = [];
    vm.entries = [];
    vm.showUser = StorageService.isAdmin();
    vm.loading = false;

    vm.searchUsers = function(query) {
      return UsersService.search(query).then(function(response) {
        return response.data.users;
      });
    };

    vm.submit = function() {
      var params = {};

      if (vm.to) {
        params.to = moment(vm.to).toISOString().substring(0, 10);
      }

      if (vm.from) {
        params.from = moment(vm.from).toISOString().substring(0, 10);
      }

      if (vm.user && vm.user.id) {
        params.userId = vm.user.id;
      }

      vm.loading = true;
      ReportsService.averageSpeedAndDistancePerWeek(params)
        .then(function(response) {
          var labels = [];
          var averageSpeeds = [];
          var distances = [];

          response.data.forEach(function(entry) {
            labels.push(entry.week);
            averageSpeeds.push(entry.averageSpeed.toFixed(2));
            distances.push((entry.distance / 1000).toFixed(2));
          });

          vm.labels = labels;
          vm.series = ['Average Speed (km/h)', 'Distance (km)'];
          vm.data = [averageSpeeds, distances];
          vm.entries = response.data;

          if (response.data.length < 1) {
            vm.flash = 'No entries found.';
          } else {
            vm.flash = '';
          }
        })
        .finally(function() {
          vm.loading = false;
        });
    };
  });