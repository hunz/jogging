angular.module('jogging').factory(
  'TokenInterceptor',
  function($q, $rootScope, StorageService) {
    return {
      request: function(request) {
        if (StorageService.hasToken()) {
          request = request || {};
          request.headers = request.headers || {};
          request.headers['Jogging-Token'] = StorageService.getToken();
        }
        return request;
      },
      responseError: function(response) {
        if (response.status == 401) {
          StorageService.removeSession();
        }
        $rootScope.$broadcast('http.error', response);
        return $q.reject(response);
      }
    };
  });

angular.module('jogging').config(
  function($httpProvider) {
    $httpProvider.interceptors.push('TokenInterceptor');
  });