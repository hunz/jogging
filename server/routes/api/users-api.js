var express = require('express');
var path = require('path');

var controller = require(path.join('..', '..', 'controllers', 'controller'));
var users = require(path.join('..', '..', 'controllers', 'users-controller'));

var requireAuthentication = require(path.join('..', '..', 'middlewares', 'require-authentication-middleware'));
var contract = require(path.join('..', '..', 'middlewares', 'contract-middleware'));

var router = express.Router();

/* List users */
router.get('/',
  requireAuthentication,
  contract({
    query: {
      optional: ['search']
    }
  }),
  function(req, res, next) {
    var token = req.joggingToken;
    var params = req.joggingParams;

    controller.withConnection(res, function(db) {
      return users.list(db, token, params);
    });
  });

/* Create user */
router.post('/',
  contract({
    headers: {
      optional: ['jogging-token']
    },
    body: {
      required: ['username', 'password', 'passwordConfirmation'],
      optional: ['role']
    }
  }),
  function(req, res, next) {
    var params = req.joggingParams;
    var token;
    var user;

    if (params.password !== params.passwordConfirmation) {
      return res.status(400).json({ error: 'invalid_param', invalid: 'passwordConfirmation' });
    }

    token = params['jogging-token'];

    user = {
      username: params.username,
      password: params.password,
      role: params.role,
    };

    controller.withConnection(res, function(db) {
      return users.create(db, token, user);
    });
  });

/* Read user */
router.get('/:id',
  requireAuthentication,
  function(req, res, next) {
    var token = req.joggingToken;
    var id = req.params['id'];

    controller.withConnection(res, function(db) {
      return users.read(db, token, id);
    });
  });

/* Update user */
router.put('/:id',
  requireAuthentication,
  contract({
    body: {
      optional: ['password', 'passwordConfirmation', 'role']
    }
  }),
  function(req, res, next) {
    var token = req.joggingToken;
    var id = req.params['id'];
    var params = req.joggingParams;

    if (typeof params.password !== 'undefined' && params.password !== params.passwordConfirmation) {
      return res.status(400).json({ error: 'invalid_param', invalid: 'passwordConfirmation' });
    }

    controller.withConnection(res, function(db) {
      return users.update(db, token, id, params);
    });
  });

/* Delete user */
router.delete('/:id',
  requireAuthentication,
  function(req, res, next) {
    var token = req.joggingToken;
    var id = req.params['id'];

    controller.withConnection(res, function(db) {
      return users.destroy(db, token, id);
    });
  });

module.exports = router;

