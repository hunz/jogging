/**
 * @swagger
 * resourcePath: /api/reports
 * description: Reports
 */

var express = require('express');
var path = require('path');

var controller = require(path.join('..', '..', 'controllers', 'controller'));
var reports = require(path.join('..', '..', 'controllers', 'reports-controller'));

var requireAuthentication = require(path.join('..', '..', 'middlewares', 'require-authentication-middleware'));
var contract = require(path.join('..', '..', 'middlewares', 'contract-middleware'));

var router = express.Router();


/**
 * @swagger
 * path: /api/reports/average-speed-and-distance-per-week
 * operations:
 *  - httpMethod: GET
 *    summary: Get the average speed and total distance per week
 *    parameters:
 *      - name: Jogging-Token
 *        paramType: header
 *        required: true
 *        dataType: string
 *      - name: from
 *        description: Start date to filter report result
 *        paramType: query
 *        required: false
 *        dataType: date
 *      - name: to
 *        description: End date to filter report result
 *        paramType: query
 *        required: false
 *        dataType: date
 *      - name: userId
 *        description: ID of User to filter report result
 *        paramType: query
 *        required: false
 *        dataType: string
 *    responses:
 *      200:
 *        reason: ok
 *        description: An array of time entries
 *        schema:
 *          type: string
 */
router.get('/average-speed-and-distance-per-week',
  requireAuthentication,
  contract({
    query: {
      optional: ['from', 'to', 'userId']
    },
    dates: ['from', 'to']
  }),
  function(req, res, next) {
    var token = req.joggingToken;
    var params = req.joggingParams;

    return controller.withConnection(res, function(db) {
      return reports.averageSpeedAndDistancePerWeek(db, token, params);
    });
  });

/**
 * @swagger
 * models:
 *  TimeAverageEntry:
 *    id: TimeAverageEntry
 *    properties:
 *      week:
 *        type: String
 *      averageSpeed:
 *        type: String
 *      distance:
 *        type: String
 */

module.exports = router;
