var express = require('express');
var path = require('path');

var controller = require(path.join(__dirname, '..', '..', 'controllers', 'controller'));
var sessions = require(path.join(__dirname, '..', '..', 'controllers', 'session-controller'));

var requireAuthentication = require(path.join('..', '..', 'middlewares', 'require-authentication-middleware'));
var contract = require(path.join('..', '..', 'middlewares', 'contract-middleware'));

var SEVEN_DAYS = 7 * 24 * 60 * 60 * 1000;

var router = express.Router();

/* Create session (Login) */
router.post('/',
  contract({
    body: {
      required: ['username', 'password']
    }
  }),
  function(req, res, next) {
    var params = req.joggingParams;
    var username = params.username;
    var password = params.password;

    controller.withConnection(res, function(db) {
      return sessions.create(db, username, password);
    });
  });

/* Delete session (Logout) */
router.delete('/',
  requireAuthentication,
  function(req, res, next) {
    var token = req.joggingToken;

    controller.withConnection(res, function(db) {
      return sessions.destroy(db, token);
    });
  });

module.exports = router;
