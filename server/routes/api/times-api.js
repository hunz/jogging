var express = require('express');
var pg = require('pg');
var path = require('path');

var controller = require(path.join(__dirname, '..', '..', 'controllers', 'controller'));
var times = require(path.join(__dirname, '..', '..', 'controllers', 'times-controller'));

var requireAuthentication = require(path.join('..', '..', 'middlewares', 'require-authentication-middleware'));
var contract = require(path.join('..', '..', 'middlewares', 'contract-middleware'));

var router = express.Router();

/* List times */
router.get('/', 
  requireAuthentication,
  contract({
    query: {
      optional: ['userId', 'from', 'to', 'lastSeenId', 'size']
    }
  }),
  function(req, res, next) {
    var token = req.joggingToken;
    var params = req.joggingParams;

    controller.withConnection(res, function(db) {
      return times.list(db, token, params);
    });
  });

/* Create time */
router.post('/',
  requireAuthentication,
  contract({
    body: {
      required: ['moment', 'distance', 'time'],
      optional: ['userId'],
    },
    dates: ['moment']
  }),
  function(req, res, next) {
    var token = req.joggingToken;
    var params = req.joggingParams;

    controller.withConnection(res, function(db) {
      return times.create(db, token, params);
    });
  });

/* Read time */
router.get('/:id',
  requireAuthentication,
  function(req, res, next) {
    var token = req.joggingToken;
    var id = req.params['id'];

    controller.withConnection(res, function(db) {
      return times.read(db, token, id);
    });
  });

/* Update time */
router.put('/:id',
  requireAuthentication,
  contract({
    body: {
      optional: ['moment', 'distance', 'time', 'userId'],
    },
    dates: ['moment']
  }),
  function(req, res, next) {
    var token = req.joggingToken;
    var id = req.params['id'];
    var params = req.joggingParams;

    controller.withConnection(res, function(db) {
      return times.update(db, token, id, params);
    });
  });

/* Delete time */
router.delete('/:id',
  requireAuthentication,
  function(req, res, next) {
    var token = req.joggingToken;
    var id = req.params['id'];

    controller.withConnection(res, function(db) {
      return times.destroy(db, token, id);
    });
  });

module.exports = router;

