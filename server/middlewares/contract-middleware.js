function section(config, configName, req, reqName, output) {
  var section;

  if (typeof config[configName] === 'undefined') {
    return;
  }
  section = config[configName];

  if (typeof section.required !== 'undefined') {
    for (i = 0; i < section.required.length; i += 1) {
      name = section.required[i];

      if (typeof req[reqName][name] === 'undefined') {
        throw { error: 'missing_param', missing: name };
      }

      output[name] = req[reqName][name];
    }
  }

  if (typeof section.optional !== 'undefined') {
    for (i = 0; i < section.optional.length; i += 1) {
      name = section.optional[i];

      if (typeof req[reqName][name] !== 'undefined') {
        output[name] = req[reqName][name];
      }
    }
  }
}

module.exports = function(config) {
  return function(req, res, next) {
    var params = {};

    try {
      section(config, 'body', req, 'body', params);
      section(config, 'query', req, 'query', params);
      section(config, 'headers', req, 'headers', params);

      if (typeof section.dates !== 'undefined') {
        for (i = 0; i < section.dates.length; i += 1) {
          name = section.dates[i];

          if (typeof output[name] !== 'undefined') {
            output[name] = moment(output[name], 'YYYY-MM-DD');
            if (!output[name].isValid()) {
              throw { error: 'invalid_param', invalid: name, expected: 'date' };
            }
            output[name] = output[name].format('YYYY-MM-DD');
          }
        }
      }
    } catch(error) {
      return res.status(400).json(error);
    }

    req.joggingParams = params;

    return next();
  };
};
