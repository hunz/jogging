module.exports = function(req, res, next) {
  if (typeof req.headers['jogging-token'] === 'undefined') {
    return res.status(401).json({ error: 'missing_header', missing: 'jogging-token' });
  }
  req.joggingToken = req.headers['jogging-token'];
  return next();
};
