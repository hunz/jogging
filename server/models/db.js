var Promise = require('promise');
var pg = require('pg');
var path = require('path');

var env = require(path.join('..', 'config', 'environment'));

function create(conn, done) {
  return new DBClient(conn, done);
}

function DBClient(conn, done) {
  this.conn = conn;
  this.done = done;
}

DBClient.prototype.query = function(sql, args, constructor) {
  var db = this;

  if (env.log.database) {
    console.log('>', sql, '|', args);
  }

  return new Promise(function(fulfill, reject) {
    var results = [];
    var query = db.conn.query(sql, args);
    query.on('row', function(row) {
      results.push(new constructor(db, row));
    });
    query.on('error', function(error) {
      reject(error);
    });
    query.on('end', function() {
      fulfill(results);
    });
  });
};

DBClient.prototype.first = function(sql, args, constructor) {
  var db = this;

  if (env.log.database) {
    console.log('>', sql, '|', args);
  }

  return new Promise(function(fulfill, reject) {
    var results = [];
    var query = db.conn.query(sql, args);
    query.on('row', function(row) {
      if (results.length >= 1) {
        return;
      }
      results.push(new constructor(db, row));
    });
    query.on('error', function(error) {
      reject(error);
    });
    query.on('end', function() {
      if (results.length < 1) {
        fulfill();
      } else {
        fulfill(results[0]);
      }
    });
  });
};

DBClient.prototype.execute = function(sql, args) {
  var db = this;

  if (env.log.database) {
    console.log('>', sql, '|', args);
  }

  return new Promise(function(fulfill, reject) {
    var query = db.conn.query(sql, args);
    query.on('error', function(error) {
      reject(error);
    });
    query.on('end', function() {
      fulfill();
    });
  });
};

DBClient.prototype.update = function(table, where, changes) {
  var key, value;
  var sets = [];
  var conditionals = [];
  var args = [];
  var i = 1;
  var sql;

  for (key in changes) {
    if (changes.hasOwnProperty(key)) {
      sets.push(key + ' = $' + i);
      i += 1;
      args.push(changes[key]);
    }
  }

  for (key in where) {
    if (where.hasOwnProperty(key)) {
      conditionals.push(key + ' = $' + i);
      i += 1;
      args.push(where[key]);
    }
  }

  sql = 'UPDATE ' + table + ' SET ' + sets.join(', ') + ' WHERE ' + conditionals.join(' AND ') + ';';

  return this.execute(sql, args);
}

DBClient.prototype.done = function() {
  this.done();
};

module.exports = {
  create: create
};

