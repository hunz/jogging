var moment = require('moment');

function all(db, params) {
  var sql = 'SELECT times.*, users.username FROM times JOIN users ON users.id = times.user_id';
  var args = [];
  var constraints = [];

  if (params.userId) {
    args.push(params.userId);
    constraints.push('times.user_id = $' + args.length);
  }

  if (params.from) {
    args.push(params.from);
    constraints.push('times.moment >= $' + args.length);
  }

  if (params.to) {
    args.push(params.to);
    constraints.push('times.moment <= $' + args.length);
  }

  if (params.lastSeen) {
    args.push(params.lastSeen.moment);
    args.push(params.lastSeen.user.username);
    args.push(params.lastSeen.id);
    constraints.push(
      '(times.moment < $' + (args.length - 2) +
        ' OR (times.moment = $' + (args.length - 2) +
          ' AND (users.username > $' + (args.length - 1) +
            ' OR (users.username = $' + (args.length - 1) +
              ' AND times.id > $' + (args.length) +
            ')' +
          ')' +
        ')' +
      ')'
    );
  }

  if (constraints.length > 0) {
    sql += ' WHERE ' + constraints.join(' AND ');
  }

  sql += ' ORDER BY times.moment DESC, users.username ASC, times.id ASC';

  if (params.size) {
    sql += ' LIMIT ' + parseInt(params.size);
  }

  return db.query(sql, args, Time);
}

function find(db, id) {
  var sql = 'SELECT times.*, users.username FROM times JOIN users ON users.id = times.user_id WHERE times.id = $1 LIMIT 1';
  var args = [id];
  return db.first(sql, args, Time);
}

function create(db, time) {
  var sql = 'INSERT INTO times(user_id, moment, distance, time) VALUES ($1, $2, $3, $4) RETURNING *';
  var timeMoment = moment(time.moment, 'YYYY-MM-DD').toISOString().substring(0, 10);
  var args = [time.userId, timeMoment, time.distance, time.time];
  return db.first(sql, args, Time);
}

function update(db, id, changes) {
  var sets = {};
  if (typeof changes.userId !== 'undefined') {
    sets['user_id'] = changes.userId;
  }
  if (typeof changes.moment !== 'undefined') {
    sets['moment'] = moment(changes.moment, 'YYYY-MM-DD').format('YYYY-MM-DD');
  }
  if (typeof changes.distance !== 'undefined') {
    sets['distance'] = changes.distance;
  }
  if (typeof changes.time !== 'undefined') {
    sets['time'] = changes.time;
  }
  return db.update('times', { id: id }, sets);
}

function destroy(db, id) {
  var sql = 'DELETE FROM times WHERE id = $1';
  var args = [id];
  return db.execute(sql, args);
}

function destroyByUserId(db, userId) {
  var sql = 'DELETE FROM times WHERE user_id = $1';
  var args = [userId];
  return db.execute(sql, args);
}

function Time(db, row) {
  this.id = row.id;
  this.userId = row.user_id;
  this.moment = moment(row.moment).toISOString().substring(0, 10);
  this.distance = row.distance;
  this.time = row.time;

  this.averageSpeed = (this.distance / 1000) / (this.time / 60);

  if (row.username) {
    this.user = {
      id: row.user_id,
      username: row.username
    };
  }
}

module.exports = {
  all: all,
  find: find,
  create: create,
  update: update,
  destroy: destroy,
  destroyByUserId: destroyByUserId
};
