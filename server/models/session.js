var md5 = require('md5');

function create(db, session) {
  var sql = 'INSERT INTO sessions (user_id, token, timeout) VALUES ($1, $2, $3)';
  var args = [session.userId, md5(session.token), session.timeout];
  return db.execute(sql, args);
}

function findByToken(db, token) {
  var sql = 'SELECT * FROM sessions WHERE token = $1 LIMIT 1';
  var args = [md5(token)];
  return db.first(sql, args, Session).then(function(session) {
    if (!session) {
      return;
    }

    if (!session.timeout || session.timeout.getTime() < new Date().getTime()) {
      return destroyByToken(db, token);
    }

    return session;
  });
}

function destroyByToken(db, token) {
  var sql = 'DELETE FROM sessions WHERE token = $1';
  var args = [md5(token)];
  return db.execute(sql, args);
}

function destroyByUserId(db, userId) {
  var sql = 'DELETE FROM sessions WHERE user_id = $1';
  var args = [userId];
  return db.execute(sql, args);
}

function Session(db, row) {
  this.id = row.id;
  this.userId = row.user_id;
  this.token = row.token;
  this.timeout = row.timeout;
}

module.exports = {
  create: create,
  findByToken: findByToken,
  destroyByToken: destroyByToken,
  destroyByUserId: destroyByUserId
};
