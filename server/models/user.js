var md5 = require('md5');

var roles = {
  admin: 'A',
  userManager: 'U',
  common: 'C'
};

function all(db, config) {
  var sql = 'SELECT * FROM users';
  var args = [];

  if (config && config.search) {
    sql += ' WHERE username LIKE $1';
    args.push('%' + config.search.toLowerCase().replace(/\s+/g, '%') + '%');
  }

  return db.query(sql, args, User);
}

function find(db, id) {
  var sql = 'SELECT * FROM users WHERE id = $1 LIMIT 1';
  var args = [id];
  return db.first(sql, args, User);
}

function findByUsername(db, username) {
  var sql = 'SELECT * FROM users WHERE username = $1 LIMIT 1';
  var args = [username];
  return db.first(sql, args, User);
}

function create(db, user) {
  var sql = 'INSERT INTO users(username, password, role) VALUES ($1, $2, $3) RETURNING *';
  var role = user.role ? user.role : roles.common;
  var args = [user.username, md5(user.password), role];
  return db.first(sql, args, User).catch(function(error) {
    if (error.code === '23505') {
      throw 'duplicate_username';
    }
    throw error;
  });
}

function update(db, id, changes) {
  var sets = {};
  if (typeof changes['role'] !== 'undefined') {
    sets['role'] = changes['role'];
  }
  if (typeof changes['password'] !== 'undefined') {
    sets['password'] = md5(changes['password']);
  }
  return db.update('users', { id: id }, sets);
}

function destroy(db, id) {
  var sql = 'DELETE FROM users WHERE ID = $1';
  var args = [id];
  return db.execute(sql, args);
}

function User(db, row) {
  this.id = row.id;
  this.username = row.username;
  this.password = row.password;
  this.role = row.role;
}

User.prototype.isAdmin = function() {
  return this.role === roles.admin;
};

User.prototype.isUserManager = function() {
  return this.role === roles.userManager;
};

module.exports = {
  all: all,
  find: find,
  findByUsername: findByUsername,
  create: create,
  update: update,
  destroy: destroy
};

