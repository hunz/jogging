var Promise = require('promise');
var path = require('path');

var users = require(path.join(__dirname, '..', 'models', 'user'));
var times = require(path.join(__dirname, '..', 'models', 'time'));
var sessions = require(path.join(__dirname, '..', 'models', 'session'));

function list(db, token, params) {
  if (!token) {
    return Promise.reject('unauthorized');
  }

  if (!params.size) {
    params.size = 70;
  }

  return sessions.findByToken(db, token).then(function(session) {
    if (!session) {
      throw 'unauthorized';
    }
    return users.find(db, session.userId);
  }).then(function(currentUser) {
    if (!currentUser.isAdmin()) {
      if (!params.userId) {
        params.userId = currentUser.id;
      } else if (params.userId != currentUser.id) {
        throw 'forbidden';
      }
    }
    if (params.lastSeenId) {
      return times.find(db, params.lastSeenId);
    }
    return false;
  }).then(function(lastSeen) {
    params.lastSeen = lastSeen;
    return times.all(db, params);
  }).then(function(records) {
    return { times: records };
  });
}

function create(db, token, record) {
  if (!token) {
    return Promise.reject('unauthorized');
  }

  return sessions.findByToken(db, token).then(function(session) {
    if (!session) {
      throw 'unauthorized';
    }
    return users.find(db, session.userId);
  }).then(function(currentUser) {
    if (!record.userId) {
      record.userId = currentUser.id;
    }
    if (!currentUser.isAdmin() && record.userId != currentUser.id) {
      throw 'forbidden';
    }
    return times.create(db, record);
  }).then(function(createdTime) {
    return times.find(db, createdTime.id);
  }).then(function(createdTime) {
    return { status: 'created', time: createdTime };
  });
}

function read(db, token, id) {
  if (!token) {
    return Promise.reject('unauthorized');
  }

  return sessions.findByToken(db, token).then(function(session) {
    if (!session) {
      throw 'unauthorized';
    }
    return users.find(db, session.userId);
  }).then(function(currentUser) {
    return times.find(db, id).then(function(time) {
      if (!time) {
        throw 'not_found';
      }
      if (time.userId != currentUser.id && !currentUser.isAdmin()) {
        throw 'forbidden';
      }
      return { time: time };
    });
  });
}

function update(db, token, id, changes) {
  if (!token) {
    return Promise.reject('unauthorized');
  }

  return sessions.findByToken(db, token).then(function(session) {
    if (!session) {
      throw 'unauthorized';
    }
    return users.find(db, session.userId);
  }).then(function(currentUser) {
    if (typeof changes.userId !== 'undefined' && !currentUser.isAdmin()) {
      throw 'forbidden';
    }
    return times.find(db, id).then(function(time) {
      if (currentUser.id != time.userId && !currentUser.isAdmin()) {
        throw 'forbidden';
      }
    });
  }).then(function() {
    return times.update(db, id, changes);
  }).then(function() {
    return times.find(db, id);
  }).then(function(time) {
    return { time: time };
  });
}

function destroy(db, token, id, password) {
  if (!token) {
    return Promise.reject('unauthorized');
  }

  return sessions.findByToken(db, token).then(function(session) {
    if (!session) {
      throw 'unauthorized';
    }
    return users.find(db, session.userId);
  }).then(function(currentUser) {
    return times.find(db, id).then(function(time) {
      if (!time) {
        return;
      }
      if (time.userId != currentUser.id && !currentUser.isAdmin()) {
        throw 'forbidden';
      }
      return times.destroy(db, id);
    });
  });
}

module.exports = {
  list: list,
  create: create,
  read: read,
  update: update,
  destroy: destroy
};

