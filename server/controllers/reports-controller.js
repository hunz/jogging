var Promise = require('promise');
var moment = require('moment');
var path = require('path');

var users = require(path.join(__dirname, '..', 'models', 'user'));
var times = require(path.join(__dirname, '..', 'models', 'time'));
var sessions = require(path.join(__dirname, '..', 'models', 'session'));

function averageSpeedAndDistancePerWeek(db, token, params) {
  if (!token) {
    return Promise.reject('unauthorized');
  }

  return sessions.findByToken(db, token).then(function(session) {
    if (!session) {
      throw 'unauthorized';
    }
    return users.find(db, session.userId);
  }).then(function(currentUser) {
    if (!params.userId) {
      params.userId = currentUser.id;
    } else if (params.userId != currentUser.id && !currentUser.isAdmin()) {
      throw 'forbidden';
    }

    return times.all(db, params);
  }).then(function(entries) {
    var weeks, weeksData, result;

    if (!entries || entries.length < 1) {
      return entries;
    }

    weeks = [];
    weeksData = {};
    entries.forEach(function(entry) {
      var entryMoment = moment(entry.moment);
      var isoWeekYear = entryMoment.isoWeekYear();
      var isoWeek = entryMoment.isoWeek();
      if (isoWeek < 10) {
        isoWeek = '0' + isoWeek;
      }
      var weekNumber = isoWeekYear + '-' + isoWeek;
      var week = weeksData[weekNumber];
      if (!week) {
        week = { entries: [] };
        weeksData[weekNumber] = week;
        weeks.push(weekNumber);
      }
      week.entries.push(entry);
    });

    weeks.reverse();

    result = [];
    weeks.forEach(function(weekNumber) {
      var data = weeksData[weekNumber];
      var timesSum = 0;
      var distancesSum = 0;
      data.entries.forEach(function(entry) {
        timesSum += (entry.time || 0);
        distancesSum += (entry.distance || 0);
      });
      result.push({
        week: weekNumber,
        averageSpeed: (distancesSum / 1000) / (timesSum / 60),
        distance: distancesSum
      });
    });

    return result;
  });
}

module.exports = {
  averageSpeedAndDistancePerWeek: averageSpeedAndDistancePerWeek
};
