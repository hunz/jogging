var uuid = require('node-uuid');
var md5 = require('md5');
var path = require('path');

var users = require(path.join(__dirname, '..', 'models', 'user'));
var sessions = require(path.join(__dirname, '..', 'models', 'session'));

var SEVEN_DAYS = 7 * 24 * 60 * 60 * 1000;

function createSession(db, username, password) {
  var token = uuid.v4();
  var timeout = new Date(new Date().getTime() + SEVEN_DAYS);
  var passwordHash = md5(password);

  return users.findByUsername(db, username).then(function(user) {
    var session;

    // FIXME Constant time checking for user password hash
    if (!user || user.password !== passwordHash) {
      throw { error: 'forbidden', message: 'invalid_username_or_password' };
    }

    session = {
      userId: user.id,
      token: token,
      timeout: timeout
    };

    return sessions.create(db, session).then(function() {
      return { userId: user.id, username: username, token: token, role: user.role };
    });
  });
}

function destroySession(db, token) {
  return sessions.destroyByToken(db, token);
}

module.exports = {
  create: createSession,
  destroy: destroySession
};

