var pg = require('pg');
var path = require('path');

var env = require(path.join(__dirname, '..', 'config', 'environment'));
var db = require(path.join(__dirname, '..', 'models', 'db'));

function withConnection(res, callback) {
  pg.connect(env.databaseUrl, function(err, client, done) {
    if (err) {
      done();
      console.log(err);
      return res.status(500).json({ error: 'internal' });
    }

    callback(db.create(client)).then(function(response) {
      if (!response) {
        res.status(204);
      } else if (response.status) {
        switch (response.status) {
          case 'created':
            res.status(201);
            break;

          default:
            return;
        }
        delete response.status;
      }
      return res.json(response);
    }, function(error) {
      if (env.log.controllerErrors) {
        console.log(error);
      }

      if (typeof error !== 'object') {
        error = { error: error };
      }

      switch (error.error) {
        case 'unauthorized':
          res.status(401);
          break;

        case 'forbidden':
          res.status(403);
          break;

        case 'not_found':
          res.status(404);
          break;

        default:
          res.status(400);
          break;
      }

      return res.json(error);
    }).finally(function() {
      done();
    });
  });
}

module.exports = {
  withConnection: withConnection
};
