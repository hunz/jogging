var Promise = require('promise');
var md5 = require('md5');
var path = require('path');

var users = require(path.join(__dirname, '..', 'models', 'user'));
var sessions = require(path.join(__dirname, '..', 'models', 'session'));
var times = require(path.join(__dirname, '..', 'models', 'time'));

function list(db, token, config) {
  if (!token) {
    return Promise.reject('unauthorized');
  }

  return sessions.findByToken(db, token).then(function(session) {
    if (!session) {
      throw 'unauthorized';
    }
    return users.find(db, session.userId);
  }).then(function(currentUser) {
    if (!currentUser.isAdmin() && !currentUser.isUserManager()) {
      throw 'forbidden';
    }
    return users.all(db, config);
  }).then(function(results) {
    results.forEach(function(user) {
      delete user.password;
    });
    return { users: results };
  });
}

function create(db, token, user) {
  var prepareOutput = function(createdUser) {
    delete createdUser.password;
    return {
      user: createdUser,
      status: 'created'
    };
  };

  if (user.role) {
    if (!token) {
      return Promise.reject('unauthorized');
    }

    return sessions.findByToken(db, token).then(function(session) {
      if (!session) {
        throw 'unauthorized';
      }
      return users.find(db, session.userId);
    }).then(function(currentUser) {
      if (!currentUser.isAdmin() && !currentUser.isUserManager()) {
        throw 'forbidden';
      }
      if (user.role == 'A' && !currentUser.isAdmin()) {
        throw 'forbidden';
      }
      return users.create(db, user);
    }).then(prepareOutput);
  } else {
    return users.create(db, user).then(prepareOutput);
  }
}

function read(db, token, id) {
  if (!token) {
    return Promise.reject('unauthorized');
  }

  return sessions.findByToken(db, token).then(function(session) {
    if (!session) {
      throw 'unauthorized';
    }

    return users.find(db, session.userId);
  }).then(function(currentUser) {
    if (id == currentUser.id) {
      return currentUser;
    }

    if (!currentUser.isAdmin() && !currentUser.isUserManager()) {
      throw 'forbidden';
    }

    return users.find(db, id);
  }).then(function(user) {
    if (!user) {
      throw 'not_found';
    }
    delete user.password;
    return { user: user };
  });
}

function update(db, token, id, changes) {
  if (!token) {
    return Promise.reject('unauthorized');
  }

  return sessions.findByToken(db, token).then(function(session) {
    if (!session) {
      throw 'unauthorized';
    }

    return users.find(db, session.userId);
  }).then(function(currentUser) {
    if (id == currentUser.id) {
      if (typeof changes['role'] !== 'undefined' && changes['role'] != currentUser.role) {
        throw 'forbidden';
      }
      if (typeof changes['password'] === 'undefined') {
        throw { error: 'missing_param', missing: 'password' };
      }
      return users.update(db, id, { password: changes.password });
    }

    if (!currentUser.isAdmin() && !currentUser.isUserManager()) {
      throw 'forbidden';
    }

    if (typeof changes['password'] !== 'undefined') {
      throw 'forbidden';
    }

    if (typeof changes['role'] === 'undefined') {
      throw { error: 'missing_param', missing: 'role' };
    }

    if (changes['role'] == 'A' && !currentUser.isAdmin()) {
      throw 'forbidden';
    }

    return users.update(db, id, { role: changes.role });
  }).then(function() {
    return users.find(db, id);
  }).then(function(user) {
    delete user.password;
    return { user: user };
  });
}

function destroy(db, token, id) {
  if (!token) {
    return Promise.reject('unauthorized');
  }

  return sessions.findByToken(db, token).then(function(session) {
    if (!session) {
      throw 'unauthorized';
    }

    return users.find(db, session.userId);
  }).then(function(currentUser) {
    if (id == currentUser.id) {
      throw 'forbidden';
    } else if (!currentUser.isAdmin() && !currentUser.isUserManager()) {
      throw 'forbidden';
    }

    return users.find(db, id).then(function(user) {
      if (user.role == 'A' && !currentUser.isAdmin()) {
        throw 'forbidden';
      }

      return times.destroyByUserId(db, id);
    }).then(function() {
      return sessions.destroyByUserId(db, id);
    }).then(function() {
      return users.destroy(db, id);
    });
  });
}

module.exports = {
  list: list,
  create: create,
  read: read,
  update: update,
  destroy: destroy
};

