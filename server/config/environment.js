var e = process.env.NODE_ENV || 'development';

var prod = {
  databaseUrl: process.env.DATABASE_URL,
  log: {
    databaseScripts: false,
    database: false,
    controllerErrors: true
  }
};

var dev = {
  databaseUrl: 'postgres://jogging_user:secret@localhost:5432/jogging',
  log: {
    databaseScripts: true,
    database: true,
    controllerErrors: true
  }
};

var test = {
  databaseUrl: 'postgres://jogging_user_test:secret_test@localhost:5432/jogging_test',
  log: {
    databaseScripts: false,
    database: false,
    controllerErrors: false
  }
};

switch (process.env.NODE_ENV) {
  case 'prod':
    module.exports = prod;
    break;

  case 'test':
    module.exports = test;
    break;

  case 'development':
  default:
    module.exports = dev;
    break;
}

